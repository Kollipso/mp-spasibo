<?
namespace MPSpasibo;
/*
	    ____                      __                              __       _            __ 
	   / __ \   ____ _   _____   / /_   ____     ___     _____   / /      (_)  _____   / /_
	  / /_/ /  / __ `/  / ___/  / __/  / __ \   / _ \   / ___/  / /      / /  / ___/  / __/
	 / ____/  / /_/ /  / /     / /_   / / / /  /  __/  / /     / /___   / /  (__  )  / /_  
	/_/       \__,_/  /_/      \__/  /_/ /_/   \___/  /_/     /_____/  /_/  /____/   \__/  
	                                                                                       
*/


class PartnerList{
	private $rows = array();
	private $fields = array();
	public $search = "";
	private $sheet = 0;
	private $sheet_all = 0;
	
	public function __construct($rows = array(), $sheet = 0, $sheet_all = -1){
		$this->rows = $rows;
		$this->fields = (count($rows) ? array_fill_keys(array_keys($this->rows[0]), "") : array());
		$this->sheet = $sheet;
		$this->sheet_all = $sheet_all;
	}
	
	public function showPartners(){
		$result = $this->showHeader();
		$result .= $this->showRow();
		$result .= $this->showFooter();

		return $result;
	}
	
	private function showHeader(){
		global $config, $app;
		$header = "";
		if (!empty($this->fields)){
			$header .= "<th>{$config['RU']['FIELDS']['EDIT']}</th>\n";
			foreach($this->fields as $v_name => $v_value){
				if (empty($config['partners']['view']) || !in_array($v_name, $config['partners']['view'])) continue;
				$header .= "<th>".(empty($config['RU']['FIELDS'][$v_name])?$v_name:$config['RU']['FIELDS'][$v_name])."</th>\n";
				
			}
			$header .= "<th>{$config['RU']['FIELDS']['DELETE']}</th>\n";
		}
		
		$app->view->set('%%header%%', $header);
		$app->view->set('%%search%%', $this->search);

		return $app->render('body_partners_header.html');
	}

	private function showRow(){
		global $config, $app;
		
 		if($this->rows[0]['id']){
			foreach($this->rows as $line){
				$row .= "<tr>\n";
				$row .= "<td><a href=\"/mp-spasibo/partner/edit/{$line['id']}/\">".$config['RU']['FIELDS']['EDIT_LINE']."</td>\n";
				foreach($line as $v_key => $v_value){
					if (empty($config['partners']['view']) || !in_array($v_key, $config['partners']['view'])) continue;
					if ($v_key == 'id'){
						$row .= "<td><a href=\"/mp-spasibo/partner/edit/$v_value/\">".$v_value."</td>\n";
					}elseif ($v_key == 'brand_type'){
						$row .= "<td>".$config['html']['partner']['brand_type'][$v_value]."</td>\n";
					}elseif ($v_key == 'tos_count'){
						$row .= "<td><a href='/mp-spasibo/tos/?id_tsp=".$line['id']."'>".$v_value."</a></td>\n";
					}elseif ($v_key == 'partner_type'){
						$row .= "<td>".$config['html']['partner']['partner_type'][$v_value]."</td>\n";
					}elseif ($v_key == 'category'){
						$row .= "<td>";
						$_category = explode(";", $v_value);
						foreach($config['html']['partner']['category'] as $btype => $bvalue){
							if (in_array($btype, $_category)){
								$row .= $bvalue." ";
							}
						}
						$row .= " </td>\n";
					}else{
						$row .= "<td>".$v_value."</td>\n";
					}
				}
				$row .= "<td><a href=\"/mp-spasibo/partner/delete/{$line['id']}/\" onclick=\"if (confirm('Удалить информацию?')) return true; else return false;\">".$config['RU']['FIELDS']['DELETE_LINE']."</td>\n";
				$row .= "</tr>\n";
			}
		}else{
			if (empty($config['partners']['view'])) $_count = count($this->rows[0]) + 2;
			else $_count = count($config['partners']['view']) + 2;
			$row .= "<tr><td colspan='{$_count}'>".$config['RU']['PARTNER']['EMPTY']."</td></tr>\n";

		}
		$app->view->set('%%row%%', $row);

		return $app->render('body_partners_row.html');
	}

	private function showFooter(){
		global $config, $app;

		$app->view->set('%%footer%%', '');
		$app->view->set('%%pages%%', $this->sheet_all*$config['items_per_page']);
		$app->view->set('%%page%%', $this->sheet+1);
		$app->view->set('%%items_per_page%%', $config['items_per_page']);

		return $app->render('body_partners_footer.html');	}

	public function getPartners(){
		global $config;

		$sqlParams = array();
		$sql = "
			SELECT 
			p.*
			FROM  `partners` AS p
			ORDER BY ID ASC
		";

		PDOQuery::getInstance($config['mysql_user'], $config['mysql_password'], $config['mysql_host'], $config['mysql_db']);
		$rows = PDOQuery::getInstance()->queryPrepared($sql, $sqlParams);
		$rows_count = 0;
		if ($rows === false){
			// echo PDOQuery::getInstance()->getError();
			return false;
		}else{
			$rows_count = PDOQuery::getInstance()->getRowCount();
		}

		return $rows;
	}
}
?>