<?php
namespace MPSpasibo;

/**
 * Log Writer
 *
 * This class is used by Slim_Log to write log messages to a valid, writable
 * resource handle (e.g. a file or STDERR).
 *
 * @package Slim
 * @author  Josh Lockhart
 * @since   1.6.0
 */
class LogWriter
{
    
    /**
     * @var resource
     */
    // protected $resource;

    /**
     * @var boolean
     */
    protected $isAjax;

    /**
     * @var string
     */
    protected $appLog;

    /**
     * @var string
     */
    protected $fileLog;
    
    /**
     * Constructor
     * @param  resource                  $resource
     * @param  boolean                  $isAjax
     * @throws \InvalidArgumentException If invalid resource
     */
    public function __construct($isAjax = false) {
        // if (!is_resource($resource)) {
        //     throw new \InvalidArgumentException('Cannot create LogWriter. Invalid resource handle.');
        // }
        // $this->resource = $resource;
        $this->isAjax = $isAjax;
    }
    
    /**
     * Write message
     * @param  mixed     $message
     * @param  int       $level
     * @return int|bool
     */
    public function write($message, $level = null, $ajax = false) {
        global $config;

        if ($this->isAjax || $ajax){
            if (is_array($message)){
                $status = $this->clearString($config['RU']['STATUS'][$level]);
                $message_str = $this->clearString(implode(" ", array(date("H:i:s"), (empty($message['line']) ? "" : " [" . $message['line'] . "]"), $message['message'])));
                echo 'putMessage('.$level.',"'.$status.'","'.$message_str.'");';
            }
        }

        if (!empty($level)){
            $status = strip_tags($this->clearString($config['RU']['STATUS'][$level]));
        }else{
            $status = "";
        }
        if (is_array($message)){
            $message_str = $this->clearString(implode(";", array(date("d.m.Y H:i:s"), $status, $message['message'], $message['line'])));
        }else{
            $message_str = $this->clearString(implode(";", array(date("d.m.Y H:i:s"), $status, (string)$message, "" . PHP_EOL)));
        }

        if (!empty($this->getAppLog())){
            $handle = fopen($this->getAppLog(), 'a+');
            fwrite($handle, $message_str . PHP_EOL);
            fclose($handle);
        }
        if (!empty($this->getFileLog())){
            $handle = fopen($this->getFileLog(), 'a+');
            fwrite($handle, $message_str . PHP_EOL);
            fclose($handle);
        }
        return;
    }

    /**
     * Remove chars from string
     * @param  string $str input string
     * @return string      clean string
     */
    public function clearString($str){
        return str_replace(array('"', "\r", "\n"), array('\"', ""), $str);
    }

    /**
     * Set app log filename
     * @param string $value filename
     */
    public function setAppLog($value = ""){
        if (!empty($value)) {
            $this->appLog = $value;
        }
    }

    /**
     * Get app log filename
     * @return [type]        filename
     */
    public function getAppLog(){
        return $this->appLog;
    }    

    /**
     * Set file log filename
     * @param string $value filename
     */
    public function setFileLog($value = ""){
        if (!empty($value)) {
            $this->fileLog = $value;
        }
    }

    /**
     * Get file log filename
     * @return [type]        filename
     */
    public function getFileLog(){
        return $this->fileLog;
    }
}