<?
namespace MPSpasibo;
use PDO;
use PDOException;

/*
	    ____  ____  ____     ____                       
	   / __ \/ __ \/ __ \   / __ \__  _____  _______  __
	  / /_/ / / / / / / /  / / / / / / / _ \/ ___/ / / /
	 / ____/ /_/ / /_/ /  / /_/ / /_/ /  __/ /  / /_/ / 
	/_/   /_____/\____/   \___\_\__,_/\___/_/   \__, /  
	                                           /____/   
*/

class PDOQuery{
	/*
		Connect to mysql information	
	*/

	private $user;
	private $pass;
	private $host;
	private $dbname;
	private $encoding;

	/*
		Database connection link
	*/
	private $dbh = null;
	
	/*
		Last result information	
	*/
	private $lastRowCount = null;
	private $lastSQL = null;
	private $lastResult = null;
	private $lastError = null;
	private $lastInsertId = null;

	protected static $instance = null;

	public static function getInstance($user = null, $pass = null, $host = null, $dbname = null, $encoding = "utf-8") {
		if (self::$instance === null) {
			self::$instance = new self();
			$object = self::$instance;
			if (!$object->isConnected() && !empty($user)){
				try{
					$object->connect($user, $pass, $host, $dbname, $encoding);
				}catch(PDOException $e){
				    $object->lastError = $e->getMessage();  
				}
			}

		}

		return self::$instance;
	}
	
	public function __construct(){
	}

	/**
	 * Connect to database
	 * @param  string
	 * @param  string
	 * @param  string
	 * @param  string
	 * @param  string
	 * @return boolean
	 */
	public function connect($user = null, $pass = null, $host = null, $dbname = null, $encoding = "utf-8"){
		if (!empty($user)){
			$this->user = $user;
			$this->pass = $pass;
			$this->host = $host;
			$this->dbname = $dbname;
			$this->encoding = $encoding;
		}

		if ($this->isConnected()){
			return true;
		}elseif (!empty($this->user) && !empty($this->host) && !empty($this->dbname)) {
			try{
				$this->dbh = new PDO("mysql:host=".$this->host.";dbname=".$this->dbname, $this->user, $this->pass, array(PDO::MYSQL_ATTR_FOUND_ROWS => true));
				$this->dbh->exec("SET NAMES UTF8");

				return true;
			}catch(PDOException $e) {  
			    $this->lastError = $e->getMessage();  
				throw new PDOException($e->getMessage(), 1);
			}			
		}
		return false;
	}

	/**
	 * Check DB connection
	 * 
	 * @return boolean
	 */
	public function isConnected(){
		return ($this->dbh === null ? false : true);
	}

	/**
	 * Clear results of prev SQL query
	 */
	private function clearResults(){
		$obj = self::$instance;

		$obj->lastSQL = null;
		$obj->lastRowCount = null;
		$obj->lastResult = null;
		$obj->lastError = null;
		$obj->lastInsertId = null;
	}

	/**
	 * Query DB
	 * 
	 * @param  string
	 * @return array()
	 */
	public static function query($sql = null, $getRowCount = true){
		if (empty($sql)){
			return false;
		}else{

			$obj = self::$instance;
			if (!$obj->connect()) return false;
			try{
				$sql = str_replace(array("\n", "\r", "\r\n", "\n\r"), "", $sql);
				$obj->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);				
				$sth = $obj->dbh->query($sql);
				$resultArray = array();
				if ($sth !== false && stripos($sql, "update") === false && stripos($sql, "insert") === false && stripos($sql, "delete") === false) {
					while ($row = $sth->fetch()) {  
						$resultArray[] = $row;
					}

					// $sqlCount = preg_replace(array("/select\s+((.|\s)+)from/i", "/(limit\s+.+)/i"), array(" count(*) ", ""), $sql);
					if ($getRowCount){
						$sqlCount = preg_replace(array("/select(.+)from/i", "/(limit\s+.+)/i"), array("SELECT count(*) FROM", ""), $sql);
		
						$sth = $obj->dbh->query($sqlCount);
					}
				}
			}catch(PDOException $exp){
				$obj->lastError = $exp->getMessage();
				return false;
			}
			$obj->lastSQL = $sql;
			$obj->lastResult = $resultArray;
			if ($getRowCount){
				$obj->lastRowCount = $sth->fetchColumn();
			}
			$obj->lastInsertId = $obj->dbh->lastInsertId();

			return $resultArray;
		}
	}

	public function getRowCount(){
		$obj = self::$instance;
		if ($obj->lastRowCount === null) return false;
		return $obj->lastRowCount;
	}

	public function getInsertId(){
		$obj = self::$instance;
		if ($obj->lastInsertId === null) return 0;
		return $obj->lastInsertId;
	}

	public function getError(){
		$obj = self::$instance;
		if ($obj->lastError === null) return false;
		return $obj->lastError." ".$obj->lastSQL;
	}

	public function queryPrepared($sql = null, $vars = array(), $getRowCount = true){
		if (empty($sql)){
			return false;
		}else{
			$obj = self::$instance;

			if (!$obj->connect()) return false;
			try{
				$sql = str_replace(array("\n", "\r", "\r\n", "\n\r"), "", $sql);

				$obj->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);				
				$sth = $obj->dbh->prepare($sql);
				if ($sth !== false) {
					$sth->setFetchMode(PDO::FETCH_ASSOC);
					if (!empty($vars))
						foreach ($vars as $key => $value) {
							if (gettype($value) == "integer"){
								$sth->bindValue($key, $value, PDO::PARAM_INT);
							}else{
								$sth->bindValue($key, $value);
							}
						}
					$_result = $sth->execute();

					$resultArray = array();
					if ($_result && stripos($sql, "update") === false && stripos($sql, "insert") === false && stripos($sql, "delete") === false){
						while ($row = $sth->fetch()) {  
							$resultArray[] = $row;
						}

						$sth = null;
						if ($getRowCount){
							$sqlCount = preg_replace(array("/select(.+)from/iU", "/(limit\s+.+)/i"), array("SELECT count(*) FROM", ""), $sql, 1);
							$sth = $obj->dbh->prepare($sqlCount);
							if ($sth !== false){
								if (!empty($vars))
									foreach ($vars as $key => $value) {
										if (gettype($value) == "integer"){
											$sth->bindValue($key, $value, PDO::PARAM_INT);
										}else{
											$sth->bindValue($key, $value);
										}
									}
					
								$sth->execute();
							}
						}
					}
				}
			}catch(PDOException $exp){
				$obj->lastError = $exp->getMessage()."".$exp->getTraceAsString();
				return false;
			}
			$obj->lastSQL = $sql;
			$obj->lastResult = $resultArray;
			if ($getRowCount){
				$obj->lastRowCount = $sth->fetchColumn();
			}
			$obj->lastInsertId = $obj->dbh->lastInsertId();

			if (stripos($sql, "update") !== false || stripos($sql, "insert") !== false || stripos($sql, "delete") !== false){
				return $_result;
			}else{
				return $resultArray;
			}
		}
	}

	public function getColumnsName($sql = ""){
		if (empty($sql)){
			return false;
		}else{
			$obj = self::$instance;

			if (!$obj->connect()) return false;
			try{
				$sql = str_replace(array("\n", "\r", "\r\n", "\n\r"), "", $sql);
				$obj->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);				
				$sth = $obj->dbh->query($sql);
				$resultArray = array();
				if ($sth !== false){
					foreach(range(0, $sth->columnCount() - 1) as $column_index){
						$column = $sth->getColumnMeta($column_index);
						$resultArray[$column["name"]] = "";
					}				}
			}catch(PDOException $exp){
				$obj->lastError = $exp->getMessage()."".$exp->getTraceAsString();
				return false;
			}

			return $resultArray;
		}
	}

    private function __clone() {
    }

    private function __wakeup() {
    } 
}	
