<?
namespace MPSpasibo;
use MPSpasibo\TOList;
use ForceUTF8\Encoding;
/*
	    ____                      __                          
	   / __ \   ____ _   _____   / /_   ____     ___     _____
	  / /_/ /  / __ `/  / ___/  / __/  / __ \   / _ \   / ___/
	 / ____/  / /_/ /  / /     / /_   / / / /  /  __/  / /    
	/_/       \__,_/  /_/      \__/  /_/ /_/   \___/  /_/     
	                                                          
*/


class Partner{
	public $values = array();
	private $fields = array();
	public $errors = array();
	
	public function __construct($values = array()){
		$this->values = $values;
		$this->fields = array_fill_keys(array_keys($this->values), "");
	}
	
	/**
	 * Show Partner form
	 * @return string html partner form
	 */
	public function showForm(){
		global $config;
		
		$form = $header = $footer = "";
		$form .= "<input type=\"hidden\" name=\"type\" value=\"partners\" />";
		$form .= "<input type=\"hidden\" name=\"action\" value=\"save\" />";
		
		if (empty($this->values) && !empty($this->fields)){ $data = $this->fields; }else{ $data = $this->values; }
		foreach($data as $v_name => $v_value){
			if ($v_name == 'id'){
				$form .= "<input type=\"hidden\" name=\"id\" value=\"$v_value\" />\n";
			}elseif ($v_name == 'update_time'){
				$form .= "<input type=\"hidden\" name=\"update_time\" value=\"LOCALTIMESTAMP()\" />\n";
			}elseif ($v_name == 'category'){
				$form .= "<input type=\"hidden\" name=\"category\" value=\"$v_value\" />\n";
				$_category = explode(";", $v_value);
				$form .= "<label>".(empty($config['RU']['FIELDS'][$v_name])?$v_name:$config['RU']['FIELDS'][$v_name])."".(in_array($v_name, $config['partner']['required'])?"*":"")."</label>&nbsp;<select id=\"$v_name\" multiple>\n";
				foreach($config['html']['partner']['category'] as $btype => $bvalue){
					if (in_array($btype, $_category)){
						$form .= "<option selected value=\"$btype\">$bvalue</option>\n";
					}else{
						$form .= "<option value=\"$btype\">$bvalue</option>\n";
					}
				}
				$form .= "</select><br>\n";
			}elseif ($v_name == 'brand_type'){
				$form .= "<label>".(empty($config['RU']['FIELDS'][$v_name])?$v_name:$config['RU']['FIELDS'][$v_name])."".(in_array($v_name, $config['partner']['required'])?"*":"")."</label>&nbsp;<select name=\"$v_name\">\n";
				$form .= "<option value=\"\"></option>\n";
				foreach($config['html']['partner']['brand_type'] as $btype => $bvalue){
					if ($btype == $v_value){
						$form .= "<option selected value=\"$btype\">$bvalue</option>\n";
					}else{
						$form .= "<option value=\"$btype\">$bvalue</option>\n";
					}
				}
				$form .= "</select><br>\n";
			}elseif ($v_name == 'partner_type'){
				$form .= "<label>".(empty($config['RU']['FIELDS'][$v_name])?$v_name:$config['RU']['FIELDS'][$v_name])."".(in_array($v_name, $config['partner']['required'])?"*":"")."</label>&nbsp;<select name=\"$v_name\">\n";
				$form .= "<option value=\"\"></option>\n";
				foreach($config['html']['partner']['partner_type'] as $btype => $bvalue){
					if ($btype == $v_value){
						$form .= "<option selected value=\"$btype\">$bvalue</option>\n";
					}else{
						$form .= "<option value=\"$btype\">$bvalue</option>\n";
					}
				}
				$form .= "</select><br>\n";
			}else{
				$form .= "<label>".(empty($config['RU']['FIELDS'][$v_name])?$v_name:$config['RU']['FIELDS'][$v_name])."".(in_array($v_name, $config['partner']['required'])?"*":"")."</label>&nbsp;<input type=\"text\" name=\"".$v_name."\" value=\"".$v_value."\" /><br>\n";
			}
		}
		
		$footer = "</br><button id=\"save\" type=\"submit\">".(empty($this->values['id'])?"Создать":"Сохранить")."</button>\n";
		if (!empty($this->values['id'])){
			$footer .= '
				<button id="delete">Удалить</button>
				<script>
					$("#delete").click(function(){
						if (confirm("Удалить информацию?")){
							$("[name=action]").val("delete");
							$("form").submit();
						}
						
						return false;
					});
				</script>
				';
		}
		$js = "";
		if (!empty($config['partner']['required'])){
			$_js = "\nvar required = new Array(\"";
			$_js .= implode('","', $config['partner']['required']);
			$_js .= "\");\n";
			$js .= <<<LST
						var all_required = true;
LST;
			$js .= $_js;
				
			$js .= <<<LST
						for (var i = 0; i < required.length; i++) {
						   if ($("[name="+required[i]+"]").val() == ""){
						   		all_required = false;
						   		$("[name="+required[i]+"]").addClass('field_error');

						   	}else{
						   		$("[name="+required[i]+"]").removeClass('field_error');
						   	}
						}
						if (!all_required){
							alert("Заполните обязательные поля");
							return false;
						}

LST;
		}
		$footer .= '
			<script>
				$("#save").click(function(){
					$("[name=action]").val("save");
					if ($("#category").val() !== null){
						$("[name=category]").val($("#category").val().join(";"));
					}else{
						$("[name=category]").val("");
					}
					'.$js.'
					return true;
				})
			</script>
			';

		$_out .= str_replace('%%header%%', $header, $config['html']['partner']['header']);
		$_out .= str_replace('%%form%%', $form, $config['html']['partner']['form']);
		$_out .= str_replace('%%footer%%', $footer, $config['html']['partner']['footer']);
		
		return $_out;
	}
	
	/**
	 * Load Partner from DB by ID to internal variable
	 * @param  int $id Partner ID
	 * @return boolean     Result of operation
	 */
	public function loadPartner($id = null){
		if (!$id) return false;
		
		global $config, $app;
		
		$sql = "SELECT 
			*
			FROM  `partners` AS f
			WHERE id = :id
		";

		$sqlParams = array(":id" => intval($id));

		PDOQuery::getInstance($config['mysql_user'], $config['mysql_password'], $config['mysql_host'], $config['mysql_db']);
		$app->getLog()->debug(array('line'=>"", 'message' => $sql));
		$this->values = PDOQuery::getInstance()->queryPrepared($sql, $sqlParams, false);
		if ($this->values === false){
			$app->getLog()->error(array('line'=>"", 'message' => PDOQuery::getInstance()->getError()));
			$this->errors[] = PDOQuery::getInstance()->getError();
			return false;
		}else{
			$this->values = $this->values[0];
		}
		$this->fields = $this->getColumnsName();

		$app->getLog()->debug(array('line'=>"", 'message' => $config['RU']['PARTNER']['FOUNDED']));
		return true;
	}

	/**
	 * Seacrh Partner in DB
	 * Filter: 
	 * 		key = field
	 * 		value = {operation}value
	 * 	 		= default, equal
	 * 	 		> more
	 * 	 		< less
	 * 	 		! not equal
	 * 	 		% like %value%
	 * 	 		& in array (N,N)
	 * 
	 * @param  array $filter Params to search
	 * @param  array $limit Limit N,N
	 * @param  string $logic Logic AND or OR
	 * @return array     Result of operation
	 */
	public function searchPartner($filter = array(), $limit = array(), $logic = "AND", $isAjax = false){
		if (empty($filter)) return false;
		
		global $config, $app;
		
		$_filter = array();
		foreach ($filter as $field => $value) {
			$operand = $value[0];
			switch ($operand) {
				case '>': //More
					$_filter[] = "".$field." > ".str_replace('>', '', $value);
					break;
				case '<': //Lower
					$_filter[] = "".$field." < ".str_replace('<', '', $value);
					break;
				case '=': //Equal
					$_filter[] = "".$field." = ".str_replace('=', '', $value);
					break;
				case '%': //Like
					$_filter[] = "".$field." LIKE %".str_replace('%', '', $value)."%";
					break;
				case '!': //Not Equal
					$_filter[] = "".$field." <> ".str_replace('<>', '', $value);
					break;
				case '&': //In array
					$_filter[] = "".$field." in (".str_replace('&', '', $value).")";
					break;
				
				default:
					$_filter[] = "".$field." = ".str_replace('=', '', $value);
					break;
			}
		}

		$_filter = implode(" ".$logic." ", $_filter);

		$_limit = "";
		if (!empty($limit) && count($limit) == 2) $_limit = "LIMIT".implode(",", $limit);

		$sql = "
			SELECT 
			*
			FROM  `partners`
			WHERE ".$_filter.
			$_limit."
			ORDER BY ID ASC
		";

		$app->getLog()->debug(array('line'=>"", 'message' => $sql));

		PDOQuery::getInstance($config['mysql_user'], $config['mysql_password'], $config['mysql_host'], $config['mysql_db']);
		$values = PDOQuery::getInstance()->query($sql);
		if ($values === false){
			$app->getLog()->error(array('line'=>"", 'message' => PDOQuery::getInstance()->getError()));
			$this->errors[] = PDOQuery::getInstance()->getError();
			return false;
		}

		$app->getLog()->debug(array('line'=>"", 'message' => $config['RU']['PARTNER']['FOUNDED']));
		return $values;
	}

	function searchPartnerByTSPName($name = ""){
		return $this->searchPartner(array("tsp_name" => "%".$name));
	}

	function searchPartnerByBrandName($name = ""){
		return $this->searchPartner(array("brand_name" => "%".$name));
	}

	function searchPartnerByTSPId($id = ""){
		return $this->searchPartner(array("id_tsp" => $id));
	}

	/**
	 * Get Partner fields from DB 
	 * @return array Array of columns
	 */
	public function getColumnsName(){
		global $config;
		
		PDOQuery::getInstance($config['mysql_user'], $config['mysql_password'], $config['mysql_host'], $config['mysql_db']);
		$columns = PDOQuery::getInstance()->getColumnsName("SELECT * FROM `partners` LIMIT 0, 1");
		if ($columns === false){
			$this->errors[] = PDOQuery::getInstance()->getError();
			return array();
		}

		$this->fields = $columns;
		return $columns;
	}

	/**
	 * Delete Partner by ID
	 * @return boolean Result of operation
	 */
	public function deletePartner(){
		$_id = intval($this->values['id']);
		if (!$_id) return false;
		
		global $config, $app;
		
		$sql = "DELETE 
			FROM `partners`
			WHERE id = :id
		";

		$sqlParams = array(":id" => intval($_id));

		PDOQuery::getInstance($config['mysql_user'], $config['mysql_password'], $config['mysql_host'], $config['mysql_db']);
		$rows = PDOQuery::getInstance()->queryPrepared($sql, $sqlParams, false);
		$rows_count = PDOQuery::getInstance()->getRowCount();
		$app->getLog()->debug(array('line'=>"", 'message' => $sql));
// $app->getLog()->debug(array('line'=>"", 'message' => print_r($rows, true) . ' count: ' . ($rows_count == 0 ? '0' : $rows_count)));
		if ($rows === false){
			$app->getLog()->error(array('line'=>"", 'message' => PDOQuery::getInstance()->getError()));
			$this->errors[] = PDOQuery::getInstance()->getError();
			return false;
		}
		
		$app->getLog()->debug(array('line'=>"", 'message' => $config['RU']['PARTNER']['DELETED']));
		return true;
	}

	/**
	 * Save Partner information
	 * @return boolean Result of operation
	 */
	public function savePartner($isAjax = false){
		global $config, $app;
		
		$_values = array();
// print_r($this->values);		

		if ($this->values['id']){
			//UPDATE
			$_update = "";
			foreach ($this->values as $v_key => $v_value){
				// $ev_value = Encoding::detectEncoding($v_value);
				// if ($ev_value != "UTF-8"){
				// 	$v_value = Encoding::toUTF8($v_value);
				// }
				if ($v_key == "id"){
					$_values[$v_key] = $v_value;
				}elseif ($v_key == "update_time"){
					$_update .= "".$v_key." = LOCALTIMESTAMP(), ";
					$_values[$v_key] = htmlentities($v_value, ENT_QUOTES, "UTF-8");
				}else{
					$_update .= "".$v_key." = '".htmlentities($v_value, ENT_QUOTES, "UTF-8")."', ";
					$_values[$v_key] = htmlentities($v_value, ENT_QUOTES, "UTF-8");
				}
			}
			$_update = substr_replace($_update, "", count($_update) - 3);
			$sql = "UPDATE `partners`
			SET 
			".$_update." 
			WHERE id = :id
			";

			$sqlParams = array(":id" => intval($this->values['id']));
			$app->getLog()->debug(array('line'=>"", 'message' => $sql));

			PDOQuery::getInstance($config['mysql_user'], $config['mysql_password'], $config['mysql_host'], $config['mysql_db']);
			$rows = PDOQuery::getInstance()->queryPrepared($sql, $sqlParams, false);
			if ($rows === false){
				$app->getLog()->error(array('line'=>"", 'message' => PDOQuery::getInstance()->getError()));
				$this->errors[] = PDOQuery::getInstance()->getError();
				return false;
			}
			$app->getLog()->debug(array('line'=>"", 'message' => $config['RU']['PARTNER']['SAVED']));
		}else{
			//ADD
			$_insert = $_insert_cols = "";
			foreach ($this->values as $v_key => $v_value){
				// $ev_value = Encoding::detectEncoding($v_value);
				// if ($ev_value != "UTF-8"){
				// 	$v_value = Encoding::toUTF8($v_value);
				// }
				$_values[$v_key] = htmlentities($v_value, ENT_QUOTES, "UTF-8");
				if ($v_key == 'id'){
					$_insert .= "NULL, ";
				}elseif($v_key == 'update_time'){
					$_insert .= "LOCALTIMESTAMP(), ";
				}else{
					$_insert .= "'".htmlentities($v_value, ENT_QUOTES, "UTF-8")."', ";
				}
				$_insert_cols .= "".$v_key.", ";
			}
			$_insert = substr_replace($_insert, "", count($_insert) - 3);
			$_insert_cols = substr_replace($_insert_cols, "", count($_insert_cols) - 3);
			$sql = "INSERT INTO `partners`
			(".$_insert_cols.")
			VALUES ( 
			".$_insert."
			)";

			$app->getLog()->debug(array('line'=>"", 'message' => $sql));
			$sqlParams = array();
			
			PDOQuery::getInstance($config['mysql_user'], $config['mysql_password'], $config['mysql_host'], $config['mysql_db']);
			$rows = PDOQuery::getInstance()->query($sql, false);
			if ($rows === false){
				$app->getLog()->error(array('line'=>"", 'message' => PDOQuery::getInstance()->getError()));
				$this->errors[] = PDOQuery::getInstance()->getError();
				return false;
			}
			
			$app->getLog()->debug(array('line'=>"", 'message' => $config['RU']['PARTNER']['SAVED'] . PDOQuery::getInstance()->getInsertId()));
			$_values['id'] = PDOQuery::getInstance()->getInsertId();
		}
				
		$this->values = $_values;
		
		return true;
	}
	
}
?>