<?php
namespace MPSpasibo;

class CustomView extends \Slim\View
{
	private $prefix = '';
	private $postfix = '';

	/**
	 * Set prefix text before main render
	 * @param  string $text Prefix text
	 * @param  boolean $cleanOldResult     Flush old prefix
	 * @return null
	 */
    public function setPrefix($text, $cleanOldResult = false){
    	$this->prefix = ($cleanOldResult ? $this->prefix . (string) $text : (string) $text );
    }

	/**
	 * Set postfix text after main render
	 * @param  string $text Postfix text
	 * @param  boolean $cleanOldResult     Flush old postfix
	 * @return null
	 */
    public function setPostfix($text, $cleanOldResult = false){
    	$this->postfix = ($cleanOldResult ? $this->postfix . (string) $text : (string) $text );
    }

    public function render($template, $data = null){
        global $config;

		$templatePathname = $this->getTemplatePathname($template);
        if (!is_file($templatePathname)){
            throw new \RuntimeException("View cannot render `$template` because the template does not exist");
        }
        $data = array_merge($this->data->all(), (array) $data);
 		$flash = $data['flash'];
        unset($data['flash']);
        $keys = array_keys($data);
        $values = array_values($data);
        ob_start();
        require $templatePathname;
        $result = ob_get_clean();
        $result = $this->prefix . $result . $this->postfix;
        $result = str_replace($keys, $values, $result);
        
        return $result;
    }
}
?>