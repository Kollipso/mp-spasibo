<?
namespace MPSpasibo;
/*
	  __________  __    _      __ 
	 /_  __/ __ \/ /   (_)____/ /_
	  / / / / / / /   / / ___/ __/
	 / / / /_/ / /___/ (__  ) /_  
	/_/  \____/_____/_/____/\__/  
	                              
*/

class TOList{
	private $rows = array();
	private $fields = array();
	public $search = "";
	public $id_tsp = "";
	private $sheet = 0;
	private $sheet_all = 0;
	
	public function __construct($rows = array(), $sheet = 0, $sheet_all = -1){
		$this->rows = $rows;
		$this->fields = (count($rows) ? array_fill_keys(array_keys($this->rows[0]), "") : array());
		$this->sheet = $sheet;
		$this->sheet_all = $sheet_all;
	}
	
	public function showTOs(){
		$result = $this->showHeader();
		$result .= $this->showRow();
		$result .= $this->showFooter();
		
		return $result;
	}
	
	private function showHeader(){
		global $config, $app;

		$header = "";
		if (!empty($this->fields)){
			$header .= "<th>{$config['RU']['FIELDS']['EDIT']}</th>\n";
			foreach($this->fields as $v_name => $v_value){
				if (empty($config['tos']['view']) || !in_array($v_name, $config['tos']['view'])) continue;
				$header .= "<th>".(empty($config['RU']['FIELDS'][$v_name])?$v_name:$config['RU']['FIELDS'][$v_name])."</th>\n";
				
			}
			$header .= "<th>{$config['RU']['FIELDS']['DELETE']}</th>\n";
		}
		
		$app->view->set('%%header%%', $header);
		$app->view->set('%%search%%', $this->search);
		$app->view->set('%%id_tsp%%', $this->id_tsp);

		return $app->render('body_tos_header.html');
	}

	private function showRow(){
		global $config, $app;
		

 		if($this->rows[0]['id']){
			foreach($this->rows as $line){
				$row .= "<tr>\n";
				$row .= "<td><a href=\"/mp-spasibo/to/edit/{$line['id']}/\">".$config['RU']['FIELDS']['EDIT_LINE']."</td>\n";
				foreach($line as $v_key => $v_value){
					if (empty($config['tos']['view']) || !in_array($v_key, $config['tos']['view'])) continue;
					if ($v_key == 'id'){
						$row .= "<td><a href=\"/mp-spasibo/to/edit/{$v_value}/\">".$v_value."</td>\n";
					}elseif ($v_key == 'id_tsp'){
						$row .= "<td><a href='/mp-spasibo/partner/edit/{$v_value}/'>[".$v_value."] ".$line['tsp_name']."</a></td>\n";
					}elseif ($v_key == 'partner_type'){
						$row .= "<td>".$config['html']['to']['partner_type'][$v_value]."</td>\n";
					}elseif ($v_key == 'category'){
						$row .= "<td>";
						$_category = explode(";", $v_value);
						foreach($config['html']['to']['category'] as $btype => $bvalue){
							if (in_array($btype, $_category)){
								$row .= $bvalue." ";
							}
						}
						$row .= " </td>\n";
					}else{
						$row .= "<td>".$v_value."</td>\n";
					}
				}							

				$row .= "<td><a href=\"/mp-spasibo/to/delete/{$line['id']}/\" onclick=\"if (confirm('Удалить информацию?')) return true; else return false;\">".$config['RU']['FIELDS']['DELETE_LINE']."</td>\n";
				$row .= "</tr>\n";
			}
		}else{
			if (empty($config['tos']['view'])) $_count = count($this->rows[0]) + 2;
			else $_count = count($config['tos']['view']) + 2;
			$row .= "<tr><td colspan='{$_count}'>".$config['RU']['TO']['EMPTY']."</td></tr>\n";

		}
		$app->view->set('%%row%%', $row);

		return $app->render('body_tos_row.html');
	}
	private function showFooter(){
		global $config, $app;

		$app->view->set('%%footer%%', '');
		$app->view->set('%%pages%%', $this->sheet_all*$config['items_per_page']);
		$app->view->set('%%page%%', $this->sheet+1);
		$app->view->set('%%items_per_page%%', $config['items_per_page']);

		return $app->render('body_tos_footer.html');
	}
}
?>