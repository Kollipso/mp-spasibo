<?
namespace MPSpasibo;
use MPSpasibo\Partner;
use MPSpasibo\PartnerList;
use ForceUTF8\Encoding;
/*
	  __________ 
	 /_  __/ __ \
	  / / / / / /
	 / / / /_/ / 
	/_/  \____/  
	             
*/

class TO{
	public $values = array();
	private $fields = array();
	public $errors = array();
	
	public function __construct($values = array()){
		$this->values = $values;
		$this->fields = array_fill_keys(array_keys($this->values), "");
	}
	
/*
	         __                  ______                   
	   _____/ /_  ____ _      __/ ____/___  _________ ___ 
	  / ___/ __ \/ __ \ | /| / / /_  / __ \/ ___/ __ `__ \
	 (__  ) / / / /_/ / |/ |/ / __/ / /_/ / /  / / / / / /
	/____/_/ /_/\____/|__/|__/_/    \____/_/  /_/ /_/ /_/ 
	                                                      
*/


	/**
	 * Show TO form
	 * @return string
	 */
	public function showForm(){
		global $config;
		
		$form = $header = $footer = "";
		$form .= "<input type=\"hidden\" name=\"type\" value=\"tos\" />";
		$form .= "<input type=\"hidden\" name=\"action\" value=\"save\" />";
		if (empty($this->values) && !empty($this->fields)){ $data = $this->fields; }else{ $data = $this->values; }
		foreach($data as $v_name => $v_value){
			if ($v_name == 'id'){
				$form .= "<input type=\"hidden\" name=\"id\" value=\"$v_value\" />\n";
			}elseif ($v_name == 'update_time'){
				$form .= "<input type=\"hidden\" name=\"update_time\" value=\"LOCALTIMESTAMP()\" />\n";
			}elseif ($v_name == 'geo_update_time'){
				$form .= "<label>".(empty($config['RU']['FIELDS'][$v_name])?$v_name:$config['RU']['FIELDS'][$v_name])."".(in_array($v_name, $config['to']['required'])?"*":"")."</label>&nbsp";
				if (empty($v_value) || $v_value == "0000-00-00 00:00:00"){
					$form .= "<input type=\"text\" name=\"geo_update_time\" value=\"\" readonly/><br>\n";
				}else{
					$form .= "<input type=\"text\" name=\"geo_update_time\" value=\"$v_value\" readonly/><br>\n";
				}
			}elseif ($v_name == 'summary_address'){
				$form .= "<label>".(empty($config['RU']['FIELDS'][$v_name])?$v_name:$config['RU']['FIELDS'][$v_name])."".(in_array($v_name, $config['to']['required'])?"*":"")."</label>&nbsp";
				$form .= "<input type=\"text\" name=\"summary_address\" value=\"$v_value\" readonly/><br>\n";
			}elseif ($v_name == 'id_tsp'){
				$pl = new PartnerList();
				$partners = $pl->getPartners();

				$form .= "<label>".(empty($config['RU']['FIELDS'][$v_name])?$v_name:$config['RU']['FIELDS'][$v_name])."".(in_array($v_name, $config['to']['required'])?"*":"")."</label>&nbsp;<select name=\"$v_name\">\n";
				$form .= "<option value=\"\"></option>\n";
				if (count($partners) > 0){
					foreach($partners as $id => $value){
						$id = $value['id'];
						if ($id == $v_value){
							$form .= "<option selected value=\"$id\">[$id] {$value['tsp_name']}</option>\n";
						}else{
							$form .= "<option value=\"$id\">[$id] {$value['tsp_name']}</option>\n";
						}
					}
				}else{
					$form .= "<option selected value=\"\">".$config['RU']['ERRORS']['NO_TSP']."</option>\n";
				}
				$form .= "</select><br>\n";
			}elseif ($v_name == 'tsp_name' || $v_name == 'brand_name'){
			}else{
				$form .= "<label>".(empty($config['RU']['FIELDS'][$v_name])?$v_name:$config['RU']['FIELDS'][$v_name])."".(in_array($v_name, $config['to']['required'])?"*":"")."</label>&nbsp;<input type=\"text\" name=\"".$v_name."\" value=\"".$v_value."\" /><br>\n";
			}
		}

		$footer = "</br><button id=\"save\" type=\"submit\">".(empty($this->values['id'])?"Создать":"Сохранить")."</button>\n";
		if (!empty($this->values['id'])){
			$footer .= '
				<button id="delete">Удалить</button>
				<script>
					$("#delete").click(function(){
						if (confirm("Удалить информацию?")){
							$("[name=action]").val("delete");
							$("form").submit();
						}
						
						return false;
					});
				</script>
				';
		}
		$js = "";
		if (!empty($config['to']['required'])){
			$_js = "\nvar required = new Array(\"";
			$_js .= implode('","', $config['to']['required']);
			$_js .= "\");\n";
			$js .= <<<LST
						var all_required = true;
LST;
			$js .= $_js;
				
			$js .= <<<LST
						for (var i = 0; i < required.length; i++) {
						   if ($("[name="+required[i]+"]").val() == ""){
						   		all_required = false;
						   	}
						}
						if (!all_required){
							alert("Заполните обязательные поля");
							return false;
						}

LST;
		}
		// $footer .= '
			// <script>
				// $("#save").click(function(){
					// $("[name=action]").val("save");
					// if ($("#category").val() !== null){
						// $("[name=category]").val($("#category").val().join(";"));
					// }else{
						// $("[name=category]").val("");
					// }
					// '.$js.'
					// return true;
				// })
			// </script>
			// ';
		$_out .= str_replace('%%header%%', $header, $config['html']['to']['header']);
		$_out .= str_replace('%%form%%', $form, $config['html']['to']['form']);
		$_out .= str_replace('%%footer%%', $footer, $config['html']['to']['footer']);
		
		return $_out;
	}
	
	/**
	 * Load TO from DB by ID to internal variable
	 * @param  int $id TO ID
	 * @return boolean     Result of operation
	 */
	public function loadTO($id = null){
		if (!$id) return false;
		
		global $config;
		
		$sql = "
			SELECT 
			t . * , p.tsp_name, p.brand_name
			FROM  `tos` AS t
			LEFT JOIN  `partners` AS p ON t.id_tsp = p.id
			WHERE t.id = :id
			ORDER BY t.ID ASC
		";

		$sqlParams = array(":id" => intval($id));

		PDOQuery::getInstance($config['mysql_user'], $config['mysql_password'], $config['mysql_host'], $config['mysql_db']);
		$this->values = PDOQuery::getInstance()->queryPrepared($sql, $sqlParams, false);
		if ($this->values === false){
			$this->errors[] = PDOQuery::getInstance()->getError();
			return false;
		}else{
			$this->values = $this->values[0];
		}
		$this->fields = $this->getColumnsName();

		return true;
	}

	/**
	 * Seacrh TO from DB
	 * Filter: 
	 * 		key = field
	 * 		value = {operation}value
	 * 	 		= default, equal
	 * 	 		> more
	 * 	 		< less
	 * 	 		! not equal
	 * 	 		% like %value%
	 * 	 		& in array (N,N)
	 * 
	 * @param  array $filter Params to search
	 * @param  array $limit Limit N,N
	 * @return array     Result of operation
	 */
	public function searchTO($filter = array(), $limit = array()){
		if (empty($filter)) return false;
		
		global $config;
		
		$_filter = $sqlParams = array();
		foreach ($filter as $field => $value) {
			$operand = $value[0];
			switch ($operand) {
				case '>': //More
					$_filter[] = "t.".$field." > :$field";
					break;
				case '<': //Lower
					$_filter[] = "t.".$field." < :$field";
					break;
				case '=': //Equal
					$_filter[] = "t.".$field." = :$field";
					break;
				case '%': //Like
					$_filter[] = "t.".$field." LIKE %:$field%";
					break;
				case '!': //Not Equal
					$_filter[] = "t.".$field." <> :$field";
					break;
				case '&': //In array
					$_filter[] = "t.".$field." in (:$field)";
					break;
				
				default:
					$_filter[] = "t.".$field." = :$field";
					break;
			}
			$sqlParams[":".$field] = str_replace(array('=', '<', '>', '!', '%', '&'), '', $value);
		}

		$_filter = implode(" AND ", $_filter);

		$_limit = "";
		if (!empty($limit) && count($limit) == 2) $_limit = "LIMIT".implode(",", $limit);

		$sql = "
			SELECT 
			t . * , p.tsp_name, p.brand_name
			FROM  `tos` AS t
			LEFT JOIN  `partners` AS p ON t.id_tsp = p.id
			WHERE ".$_filter.
			$_limit."
			ORDER BY t.ID ASC
		";

		PDOQuery::getInstance($config['mysql_user'], $config['mysql_password'], $config['mysql_host'], $config['mysql_db']);
		$values = PDOQuery::getInstance()->queryPrepared($sql, $sqlParams);
		if ($values === false){
			echo PDOQuery::getInstance()->getError();
			return false;
		}

		return $values;
	}

	/**
	 * Get TO fields from DB 
	 * @return array Array of columns
	 */
	public function getColumnsName(){
		global $config;

		PDOQuery::getInstance($config['mysql_user'], $config['mysql_password'], $config['mysql_host'], $config['mysql_db']);
		$columns = PDOQuery::getInstance()->getColumnsName("SELECT * FROM `tos` LIMIT 0,1");
		if ($rows === false){
			$this->errors[] = PDOQuery::getInstance()->getError();
			return array();
		}

		$this->fields = $columns;
		return $columns;
	}

	/**
	 * Delete TO by ID
	 * @return boolean Result of operation
	 */
	public function deleteTO(){
		$_id = intval($this->values['id']);
		if (!$_id) return false;
		
		global $config;
		
		$sql = "DELETE 
			FROM `tos`
			WHERE id = :id
		";

		$sqlParams = array(":id" => intval($_id));

		PDOQuery::getInstance($config['mysql_user'], $config['mysql_password'], $config['mysql_host'], $config['mysql_db']);
		$rows = PDOQuery::getInstance()->queryPrepared($sql, $sqlParams, false);
		$rows_count = 0;
		if ($rows === false){
			$this->errors[] = PDOQuery::getInstance()->getError();
			return false;
		}
		
		return true;
	}

	/**
	 * Save TO information
	 * @return boolean Result of operation
	 */
	public function saveTO($isAjax = false){
		global $config;
		
		$_values = array();

		if ($this->values['id']){
			//UPDATE
			$_update = "";
			foreach ($this->values as $v_key => $v_value){
				if ($v_key == "id"){
					$_values[$v_key] = (int) $v_value;
				}elseif ($v_key == "update_time"){
					$_update[] = "".$v_key." = LOCALTIMESTAMP()";
					$_values[$v_key] = htmlentities($v_value, ENT_QUOTES, "UTF-8");
				}elseif ($v_key == "geo_update_time"){
					$_update[] = "".$v_key." = LOCALTIMESTAMP()";
					$_values[$v_key] = htmlentities($v_value, ENT_QUOTES, "UTF-8");
				}elseif ($v_key == "summary_address"){
					if (empty($v_value)){
					//Always construct summary_address from data
						if (!empty($config['to']['update_rules'][$v_key])){
							$_str = "";
							foreach ($config['to']['update_rules'][$v_key] as $r_key => $r_value) {
								$_str .= $this->values[$r_value];
							}
							$_str = str_replace(array(" ", ".",",","/","-","_","(",")","*","%","!","?","+","=",":","`","'","\"","<",">","]","[","#","{","}"),
							 					"", 
							 					$_str);
							$_str = trim($_str);
							$_update[] = "".$v_key." = '".htmlentities($_str, ENT_QUOTES, "UTF-8")."'";
							$_values[$v_key] = trim(htmlentities($_str, ENT_QUOTES, "UTF-8"));
						}else{
							$_update[] = "".$v_key." = '".htmlentities($v_value, ENT_QUOTES, "UTF-8")."'";
							$_values[$v_key] = trim(htmlentities($v_value, ENT_QUOTES, "UTF-8"));
						}
					}else{
						$_update[] = "".$v_key." = '".htmlentities($v_value, ENT_QUOTES, "UTF-8")."'";
						$_values[$v_key] = trim(htmlentities($v_value, ENT_QUOTES, "UTF-8"));
					}
				}else{
					$_update[] = "".$v_key." = '".htmlentities($v_value, ENT_QUOTES, "UTF-8")."'";
					$_values[$v_key] = trim(htmlentities($v_value, ENT_QUOTES, "UTF-8"));
				}
			}

			$_update = implode(", ", $_update);
			$sql = "UPDATE `tos`
			SET 
			".$_update." 
			WHERE id = :id
			";
			if ($isAjax) echo 'putMessage(4,"'.str_replace('"','\"',$config['RU']['STATUS']['DEBUG']).'","'.str_replace(array('"', "\n", "\r", "'"),array('\"', "", "", "\'"), $sql).'");';
			$sqlParams = array(":id" => intval($this->values['id']));

			PDOQuery::getInstance($config['mysql_user'], $config['mysql_password'], $config['mysql_host'], $config['mysql_db']);
			$rows = PDOQuery::getInstance()->queryPrepared($sql, $sqlParams, false);
			if ($rows === false){
				$this->errors[] = PDOQuery::getInstance()->getError();
				return false;
			}
		}else{
			//ADD
			$_insert = $_insert_cols = "";
			foreach ($this->values as $v_key => $v_value){
				$_values[$v_key] = htmlentities($v_value, ENT_QUOTES, "UTF-8");
				if ($v_key == 'id'){
					$_insert[] = "NULL";
				}elseif($v_key == 'update_time'){
					$_insert[] = "LOCALTIMESTAMP()";
				}elseif($v_key == 'geo_update_time'){
					if (empty($v_value)){
						$_insert[] = "NULL";
					}else{
						$_insert[] = "LOCALTIMESTAMP()";
					}
				}elseif ($v_key == "summary_address"){
					if (empty($v_value)){
					//Always construct summary_address from data
						if (!empty($config['to']['update_rules'][$v_key])){
							$_str = "";
							foreach ($config['to']['update_rules'][$v_key] as $r_key => $r_value) {
								$_str .= $this->values[$r_value];
							}
							$_str = str_replace(array(" ", ".",",","/","-","_","(",")","*","%","!","?","+","=",":","`","'","\"","<",">","]","[","#","{","}"),
							 					"", 
							 					$_str);
							$_str = trim($_str);
							$_insert[] = "'".htmlentities($_str, ENT_QUOTES, "UTF-8")."'";
						}else{
							$_insert[] = "'".htmlentities($v_value, ENT_QUOTES, "UTF-8")."'";
						}
					}else{
						$_insert[] = "'".htmlentities($v_value, ENT_QUOTES, "UTF-8")."'";
					}
				}else{
					$_insert[] = "'".htmlentities($v_value, ENT_QUOTES, "UTF-8")."'";
				}
				$_insert_cols[] = "".$v_key."";
			}
			if (!in_array("geo_update_time", $_insert_cols)){
				$_insert_cols[] = "geo_update_time";
				$_insert = "LOCALTIMESTAMP()";
			}
			$_insert = implode(",", $_insert);
			$_insert_cols = implode(",", $_insert_cols);
			$sql = "INSERT INTO `tos`
			(".$_insert_cols.")
			VALUES ( 
			".$_insert."
			)";
			$sqlParams = array();
			if ($isAjax) echo 'putMessage(4,"'.str_replace('"','\"',$config['RU']['STATUS']['DEBUG']).'","'.str_replace(array('"', "\n", "\r", "'"),array('\"', "", "", "\'"), $sql).'");';
// print_r($this->values);
// exit;
			PDOQuery::getInstance($config['mysql_user'], $config['mysql_password'], $config['mysql_host'], $config['mysql_db']);
			$rows = PDOQuery::getInstance()->query($sql, false);
			if ($rows === false){
				$this->errors[] = PDOQuery::getInstance()->getError();
				return false;
			}
			
			$_values['id'] = PDOQuery::getInstance()->getInsertId();
		}
		
		$this->values = $_values;
		
		return true;
	}	
}
?>