<?php

namespace Goodby\CSV\Import\Protocol;

/**
 * Interface of the Interpreter
 */
interface InterpreterInterface
{
    /**
     * @param $line
     * @param $lines
     * @return void
     */
    public function interpret($line, $lineNumber, $lines);
}
