function putMessage(detailLog,name,text) {
    // callback, добавляет сообщения на страницу, вызывается из полученных с сервера данных
    // self.last = id;
    // console.log(detailLog, name, text);
    // var b = document.createElement('div');
    // b.innerHTML = ''+name+' <span class="message_text">'+text+'</span>';
    // console.log($("#detaillog option:selected").val(), detailLog, $("#detaillog option:selected").val() >= detailLog);
    // if ($("#detaillog option:selected").val() >= detailLog){
    if (detailLog != 8){
    //     if (!$('#flashlog').prop("checked")){
    //         $('#messages').prepend(b);
    //     }else{
    //         $('#messages').html(b);
            data = ['<tr class="log_row"><td class="log_column">' + name + '</td><td class="log_column">' + text + '</td></tr>'];
            clusterize.prepend(data);
        }
    // }
}

function updateProgress(text, current, full) {
    var progress_str = "";
    var n_full = 0;
    var n_current = 0;
    n_full = 100/full; // 100 / 6000 = 0,0167
    n_current = Math.round(n_full * current); // 0,0167 * 600 = 10 
    // console.log("n_full: "+n_full);
    // console.log("n_current: "+n_current);
    for (var i = 1; i <= n_current; i++) { 
        progress_str = progress_str + "<span class='black'></span>";
    };
    for (var i = n_current; i <= 99; i++) {
        progress_str = progress_str + "<span class='gray'></span>";
    };

    $('#progress').html("<div>" + text + " " + progress_str + " <span class='detail_progress'>" + current + "/" + full + "</span></div>");
}

function stop(){
    $("#submit").text("Обработать");
    $("#submit").prop('disabled', false);
}

function Messanger(backend_url) {
    this.last = 0;
    this.timeout = 360;
    this.comet = 0;
    this.backend_url = backend_url;
    this.stop = false;
    this.skip = 0;
    this.finish = false;
    var self = this;

    //Download file when all procedures finished
    this.downloadFile = function(){
// return;
        var path = window.location.pathname;
        if (path.search('/mp/') != -1){
            window.location = '/mp-spasibo/upload/mp/'+self.backend_url;
        }else{
            if (path.search('/terminals/') != -1){
                window.location = '/mp-spasibo/upload/terminals/'+self.backend_url;
            }
        }
    }

    this.stopConnection = function() {
        $("#submit").text("Обработка");
        $("#submit").prop('disabled', false);
        $("#input_encoding").prop("disabled", false);
        $("#export_encoding").prop("disabled", false);
        self.stop = true;
    }

    // this.putMessage = function(id,name,text) {
    //     // callback, добавляет сообщения на страницу, вызывается из полученных с сервера данных
    //     self.last = id;
    //     var b = document.createElement('div');
    //     b.innerHTML = '<span style="color: red;">'+name+'</span> '+text;
    //     $('#messages').append(b);
    // }
    
    this.parseData = function(message) {
        // простая обработка данных полученных с сервера, разбиваем строки и выполняет функции
        // var items = message.split(';');
        // if (items.length<1) return false;
        // for (var i=0;i<items.length;i++) {
        //     console.log(items[i]);
        //     eval(items[i]);
        //     // console.log(self.skip);
        // }
        var _skip = self.skip;
        eval(message);
        // self.backend_url = backend_url + '&skip=' + self.skip;
// console.log(self.backend_url);
// console.log(_skip);
// if (self.skip<_skip){
    // console.log(message);
// }
        if (!self.stop){
            setTimeout(self.connection(), 200);
        }else{
            $("#submit").text("Обработка");
            $("#submit").prop('disabled', false);
            putMessage(0,"<span class='general'>general</span>", "Обработка остановлена");
        }
    }

    this.connection = function() {
        if (self.stop){
            $("#submit").text("Обработка");
            $("#submit").prop('disabled', false);
            $("#input_encoding").prop("disabled", false);
            $("#export_encoding").prop("disabled", false);
            return;
        }
        $("#submit").text("Идёт обработка...");
        $("#submit").prop("disabled", true);
        $("#input_encoding").prop("disabled", true);
        $("#export_encoding").prop("disabled", true);

        var inputEncoding = "";
        if ($("#input_encoding option:selected").val()){
            inputEncoding = "&ie=" + $("#input_encoding option:selected").val();
        }
        var exportEncoding = "";
        if ($("#export_encoding option:selected").val()){
            exportEncoding = "&ee=" + $("#export_encoding option:selected").val();
        }
        var testmode = "";
        if ($("#testmode").prop("checked")){
            testmode = "&testmode=y";
        }
        // console.log(inputEncoding);
        // console.log(exportEncoding);
        // console.log("/mp-spasibo/mp/update/" + self.skip + "/" + self.backend_url + "?" + inputEncoding + exportEncoding + testmode);

        // здесь открывается соединение с сервером
        self.comet = $.ajax({
                type: "GET",
                url:  "/mp-spasibo/mp/update/" + self.skip + "/"+self.backend_url+"?" + inputEncoding + exportEncoding + testmode,
                data: {'skip':self.skip},
                dataType: "text",
                timeout: self.timeout*1000,
                success: self.parseData,
                error: function(){
                    // something wrong. but setInterval will set up connection automatically
                    setTimeout(self.connection, 200);
               }
            });
    }
    this.init = function() {
        //setInterval(self.connection,self.timeout*1000);
        self.connection();
    }
    this.init();
}

function upload(file) {

    var xhr = new XMLHttpRequest();

    // обработчик для закачки
    xhr.upload.onprogress = function(event) {
        // var progress = Math.floor((event.loaded / event.total) * 100);
        updateProgress("Загрузка файла:", event.loaded, event.total);
        // console.log(event.loaded + ' / ' + event.total);
    }
    this.parseData = function(message) {
        // простая обработка данных полученных с сервера, разбиваем строки и выполняет функции
        var items = message.split(';');
        if (items.length<1) return false;
        for (var i=0;i<items.length;i++) {
            eval(items[i]);
        }
    }
    // обработчики успеха и ошибки
    // если status == 200, то это успех, иначе ошибка
    xhr.onload = xhr.onerror = function() {
        if (this.status == 200) {
            var resp = JSON.parse(this.response);
            // console.log(resp);
            if (!resp.error){
                putMessage('1', '<span class="general">general</span>', 'Файл ' + resp.name + ' загружен успешно');
                var csv_file = resp.name;
                // console.log(resp);
                // console.log(csv_file);
                msg = new Messanger(csv_file);
                // console.log("success");
            }else{
                putMessage('1', '<span class="error">error</span>', 'Файл ' + resp.name + ' не загружен: ' + resp.dataUrl);
                console.log("error " + resp);
            }
        } else {
            console.log("error " + this.status);
        }
    };


    var path = window.location.pathname;
    if (path.search('/mp/') != -1){
        xhr.open("POST", "/mp-spasibo/mp/upload/", true);
    }else{
        if (path.search('/terminals/') != -1){
            xhr.open("POST", "/mp-spasibo/terminals/upload/", true);
        }else{
            console.log('NOT /mp/ NOT /terminals/'); 
            alert('Check URL');
        }
    }
    // xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
    var formData = new FormData();
    formData.append("csv_file", file);
    xhr.send(formData);
}

var msg;
var cl_data;
var clusterize;
$(document).ready(function(){
    // инициализация
    $('#submit').click(function(){
        var csv_file = $('[name=csv_file]').val();
        if (csv_file != ''){
            $("#submit").text("Идёт обработка...");
            $("#submit").prop("disabled", true);
            var file = $('[name=csv_file]').prop('files')[0];
            if (file) {
                upload(file);
            }
        }else{
            alert("Выберите файл");
        }
        return false;
    });
    $('#stop').click(function(){
        // if ($("#submit").prop("disabled")){
            // console.log(msg);
            // stop();
            if (msg){
                // console.log("stop");
                msg.stopConnection();
            }
        // }
        return false;
    });
    $('#clearlog').click(function(){
        $('#messages').html('');
        clusterize.clear();
        return false;
    });

    //Clusterize table
    if ($('#scrollArea').html()){
        data = [];
        clusterize = new Clusterize({
          rows: data,
          scrollId: 'scrollArea',
          contentId: 'contentArea'
        });
    }
});