<?
// MAIL SETTINGS
//MAIL.RU
// $config['host'] = 'smtp.mail.ru';
// $config['port'] = 465;
// $config['username'] = 'kollipso2000@mail.ru';
// $config['password'] = '';
// $config['from'] = $config['username'];
// $config['from_name'] = 'Сверка Спасибо от Сбербанка';
// $config['copy'] = 'sverka@spasibosb.ru';

$config['max_execution_time'] = 360;
$config['upload_max_filesize'] = "10M";

//Reserved Exchange
$config['host'] = '192.168.219.26';
$config['username'] = 'test@sbspasibo.ru';
$config['password'] = 'cpl$2014';
$config['port'] = 25;
$config['from'] = "sverka@spasibosb.ru";
$config['from_name'] = 'Сверка Спасибо от Сбербанка';
$config['copy'] = 'sverka@spasibosb.ru';

//Exchange
// $config['host'] = 'mail.cplsb.ru';
// $config['username'] = 'sverka';
// $config['password'] = '1qaz@WSX3edc';
// $config['port'] = 25;

// $config['blind_copy'] = '';

//MESSAGE SETTINGS
// %COMPANY% - название компании
// %BRAND% - название бренда
// %PERIOD% - период отчётности
// %CONTACT_NAME% - имя контакта
// %CONTACT_EMAIL% - email компании/бренда
// %DATA% - таблица с данными

//LANG message
//Buttons
$config['RU']['SEARCH']['LABEL'] = "Поиск";
$config['RU']['SEARCH']['BUTTON'] = "Найти";
$config['RU']['RESET']['BUTTON'] = "Сбросить";
$config['RU']['PAGINATOR']['NEXT'] = ">";
$config['RU']['PAGINATOR']['PREV'] = "<";
$config['RU']['PAGINATOR']['FIRST'] = "<<<";
$config['RU']['PAGINATOR']['LAST'] = ">>>";
$config['RU']['PAGINATOR']['GO'] = "Перейти";

//Fields name
$config['RU']['FIELDS']['id'] = "ID";
$config['RU']['FIELDS']['id_tsp'] = "ТСП ID";
$config['RU']['FIELDS']['tsp_name'] = "Имя ТСП";
$config['RU']['FIELDS']['brand_name'] = "Имя Бренда";
$config['RU']['FIELDS']['brand_type'] = "Тип магазина";
$config['RU']['FIELDS']['partner_type'] = "Тип партнёра";
$config['RU']['FIELDS']['tos_count'] = "Количество ТО";

//Files columns
$config['RU']['FIELDS']['FILENAME'] = "Filename";
$config['RU']['FIELDS']['FILE_CREATED'] = "Create Date";
$config['RU']['FIELDS']['FILE_UPDATED'] = "Updated file";
$config['RU']['FIELDS']['FILE_DELETE'] = "DELETE";
$config['RU']['FIELDS']['FILE_LOG'] = "Log file";

//Actions
$config['RU']['FIELDS']['EDIT'] = "Edit";
$config['RU']['FIELDS']['DELETE'] = "Delete";
$config['RU']['FIELDS']['EDIT_LINE'] = "EDIT";
$config['RU']['FIELDS']['DELETE_LINE'] = "DELETE";

//Errors
$config['RU']['ERRORS']['TSP_BRAND_NULL'] = "<label class=\"error\">Поля '".$config['RU']['FIELDS']['tsp_name']."' и '".$config['RU']['FIELDS']['brand_name']."' не могут быть одновременно пустыми</label>";
$config['RU']['ERRORS']['ID_TSP_NULL'] = "<label class=\"error\">Поле '".$config['RU']['FIELDS']['id_tsp']."' не может быть пустым</label>";
$config['RU']['ERRORS']['PARTNER_SQL_ERROR'] = "Ошибка при сохранении: ";
$config['RU']['ERRORS']['TO_SQL_ERROR'] = "Ошибка при сохранении: ";
$config['RU']['ERRORS']['NO_TSP'] = "- ЗАВЕДИТЕ ПАРТНЁРА -";
$config['RU']['ERRORS']['CONFIG_ADDRESS_RULES'] = "Не заданы настройки поиска адреса, проверьте настройки \$config['to']['update_rules']['summary_address'] в файле config.php";
$config['RU']['ERRORS']['TO_CACHE_FOUND_TOO_MUCH'] = "Найдено более одной записи ТО";
$config['RU']['ERRORS']['YANDEX_NO_MATCH'] = "Не найдено адресов на карте";
$config['RU']['ERRORS']['TO_CACHE_TIMEOUT'] = "Точка требует обновления гео-координат";
$config['RU']['ERRORS']['NULL_ADDRESS'] = "Адрес не указан";
$config['RU']['ERRORS']['NULL_PARTNER'] = "Партнёр не указан";
$config['RU']['ERRORS']['FAILED_SAVE'] = "Ошибка сохранения данных в БД";
$config['RU']['ERRORS']['FAILED_FIND'] = "Не найдено данных в кеше и в Yandex.Maps";
$config['RU']['ERRORS']['FILE_DELETE_FAILED'] = "Файл не удалён";
$config['RU']['ERRORS']['FILE_UPLOAD_FAILED'] = "Файл не удалось загрузить на сервер: ";
$config['RU']['ERRORS']['PARTNERS_SHOW_ERROR'] = "Не удаётся отобразить список партнёров";

// WARNINGS
$config['RU']['WARNING']['YANDEX_FOUND_TOO_MUCH'] = "Найдено более одной записи в адресах Yandex.Maps";
$config['RU']['WARNING']['NO_PARTNER'] = "Нет данных по партнёру";
$config['RU']['WARNING']['PARTNER_CREATE_CATEGORY_NULL'] = "Категория не найдена: ";


//Messages
$config['RU']['PARTNER']['SAVED'] = "Партнёр сохранён";
$config['RU']['PARTNER']['DELETED'] = "Партнёр удалён";
$config['RU']['PARTNER']['EMPTY'] = "Данных нет";
$config['RU']['PARTNER']['FOUNDED'] = "Партнёр найден";
$config['RU']['TO']['EMPTY'] = "Данных нет";
$config['RU']['TO']['SAVED'] = "ТО сохранено";
$config['RU']['TO']['DELETED'] = "ТО удалено";
$config['RU']['CSV']['PROGRESS'] = "Обработка файла:";
$config['RU']['STATUS']['DEBUG'] = "<span class='debug'>debug</span>";
$config['RU']['STATUS']['INFO'] = "<span class='general'>info</span>";
$config['RU']['STATUS']['NOTICE'] = "<span class='info'>notice</span>";
$config['RU']['STATUS']['WARNING'] = "<span class='warning'>warning</span>";
$config['RU']['STATUS']['ERROR'] = "<span class='error'>error</span>";
$config['RU']['STATUS']['CRITICAL'] = "<span class='error'>critical</span>";
$config['RU']['STATUS']['ALERT'] = "<span class='error'>alert</span>";
$config['RU']['STATUS']['EMERGENCY'] = "<span class='error'>emergency</span>";
$config['RU']['STATUS'][8] = $config['RU']['STATUS']['DEBUG'];
$config['RU']['STATUS'][7] = $config['RU']['STATUS']['INFO'];
$config['RU']['STATUS'][6] = $config['RU']['STATUS']['NOTICE'];
$config['RU']['STATUS'][5] = $config['RU']['STATUS']['WARNING'];
$config['RU']['STATUS'][4] = $config['RU']['STATUS']['ERROR'];
$config['RU']['STATUS'][3] = $config['RU']['STATUS']['CRITICAL'];
$config['RU']['STATUS'][2] = $config['RU']['STATUS']['ALERT'];
$config['RU']['STATUS'][1] = $config['RU']['STATUS']['EMERGENCY'];
$config['RU']['SELECT']['GENERAL'] = "Главные";
$config['RU']['SELECT']['INFO'] = "+info";
$config['RU']['SELECT']['WARNING'] = "+warning";
$config['RU']['SELECT']['ERROR'] = "+error";
$config['RU']['SELECT']['DEBUG'] = "+debug";
$config['RU']['INFO']['HEADER_PASSED'] = "Обработан заголовок";
$config['RU']['INFO']['LINE_PASSED'] = "Строка обработана";
$config['RU']['INFO']['FILE_PASSED'] = "Файл обработан";
$config['RU']['INFO']['FILE_UPLOADED'] = "Файл загружен";
$config['RU']['INFO']['CACHE_FOUND'] = "Найден в кеше";
$config['RU']['INFO']['YANDEX_SEARCH'] = "Поиск в Yandex.Maps";
$config['RU']['INFO']['YANDEX_FOUND'] = "Адрес найден";
$config['RU']['INFO']['START'] = "Файл поставлен в обработку";
$config['RU']['INFO']['PARTNER_CREATED'] = "Партнёр сохранён";
$config['RU']['INFO']['DOWNLOAD'] = "Ссылка на обработанный файл";
$config['RU']['INFO']['FILE_DELETE_SUCCESS'] = "Файл удалён";

// //Page HTML
// //Header
// $config['html']['page']['header'] = '<!DOCTYPE html>
// <html lang="ru">
// <head>
// 	<meta http-equiv="cache-control" content="no-cache" />
// 	<meta charset="UTF-8" />
// 	<!--meta http-equiv="content-type" content="text/html; charset=utf-8" /-->
// 	<meta http-equiv="content-language" content="ru" />
// 	<link rel="stylesheet" type="text/css" href="/mp-spasibo/style/style.css" />
// 	<link rel="stylesheet" type="text/css" href="/mp-spasibo/style/clusterize.css" />
// 	<script src="/mp-spasibo/js/jquery-2.1.3.min.js"></script>
// 	<script src="/mp-spasibo/js/messanger.js"></script>
// 	<script src="/mp-spasibo/js/clusterize.min.js"></script>
// 	<title>%%name%%</title>
// </head>

// <body>
// 	<ul>
// 		<li><a href="/mp-spasibo/mp/">Обработка файла для МП</a> | <a href="/mp-spasibo/mp/files/">Обработанные файлы</a></li>
// 		<li><a href="/mp-spasibo/terminals/">Обработка файла по терминалам</a> | <a href="/mp-spasibo/terminals/files/">Обработанные файлы</a></li>
// 		<li><a href="/mp-spasibo/partners/">Информация о партнёрах</a> | <a href="/mp-spasibo/partners/add/">Добавить нового</a></li>
// 		<li><a href="/mp-spasibo/tos/">Информация о точках</a> | <a href="/mp-spasibo/tos/add/">Добавить новую</a></li>
// 	</ul>
// ';
// //Footer
// $config['html']['page']['footer'] = '
// </body>	
// </html>
// ';

//PARTNER FORM
//Header
$config['html']['partner']['header'] = '
<form method="post" action="/mp-spasibo/partner/save/" name="form" enctype="multipart/form-data">
';
//Form
$config['html']['partner']['form'] = '
%%form%%
';
//Footer
$config['html']['partner']['footer'] = '
%%footer%%
</form>
';

//TO FORM
//Header
$config['html']['to']['header'] = '
<form method="post" action="/mp-spasibo/to/save/" name="form" enctype="multipart/form-data">
';
//Form
$config['html']['to']['form'] = '
%%form%%
';
//Footer
$config['html']['to']['footer'] = '
%%footer%%
</form>
';

//MP File Page
//Form code
$config['html']['mp']['form'] = '
	<form method="post" enctype="multipart/form-data">
		<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
		Файл с адресами: <input type="file" name="csv_file"><br><br/>

		Настройки:<br/>
		<label class="settings_row">Тестовый режим</label><input type="checkbox" name="testmode" id="testmode" value="1"><br/>
		<label class="settings_row">Короткий лог</label><input type="checkbox" name="flashlog" id="flashlog" value="1"><br/>
		<label class="settings_row">Кодировка исходного файла</label><select name="input_encoding" id="input_encoding">
			<option value="" selected>- AUTO -</option>
			<option value="utf-8">UTF-8</option>
			<option value="cp1251">CP1251 (Windows)</option>
			<option value="cp866">CP866 (MSDOS)</option>
			<option value="koi8r">KOI8R</option>
		</select><br/>
		<label class="settings_row">Кодировка конечного файла</label><select name="export_encoding" id="export_encoding">
			<option value="" selected>- AUTO -</option>
			<option value="utf-8">UTF-8</option>
			<option value="cp1251">CP1251 (Windows)</option>
			<option value="cp866">CP866 (MSDOS)</option>
			<option value="koi8r">KOI8R</option>
		</select><br/>
		<!--label class="settings_row">Детальный лог</label><select name="detaillog" id="detaillog">
			<option value="7" selected>'.$config['RU']['SELECT']['GENERAL'].'</option>
			<option value="6">'.$config['RU']['SELECT']['INFO'].'</option>
			<option value="5">'.$config['RU']['SELECT']['WARNING'].'</option>
			<option value="3">'.$config['RU']['SELECT']['ERROR'].'</option>
			<option value="8">'.$config['RU']['SELECT']['DEBUG'].'</option>
		</select><br/-->
		<label class="settings_row">Пропускать строки с ошибками</label><input type="checkbox" name="passerrors" id="passerrors" checked value="1"><br/>
		<button type="submit" name="submit" id="submit">Обработать</button>&nbsp;<button type="submit" name="stop" id="stop">Остановить</button>&nbsp;<button type="submit" name="clearlog" id="clearlog">Очистить лог</button>
	</form>
	<div id="progress"></div>
	<div id="clusterize" class="clusterize">
		<table>
			<thead class="log_header"><td>Статус</td><td>Описание</td></thead>
		</table>
		<div id="scrollArea" class="clusterize-scroll">
			<table>
				<tbody id="contentArea" class="clusterize-content">
					<tr class="clusterize-no-data">
						<td>Loading data…</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div id="messages"></div>
';

//Terminals File Page
//Form code
$config['html']['terinals']['form'] = '
	<form method="post" enctype="multipart/form-data">
		<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
		Файл с адресами: <input type="file" name="csv_file"><br><br/>

		Настройки:<br/>
		<label class="settings_row">Короткий лог</label><input type="checkbox" name="flashlog" id="flashlog" value="1"><br/>
		<label class="settings_row">Детальный лог</label><select name="detaillog" id="detaillog">
			<option value="0" selected>'.$config['RU']['SELECT']['GENERAL'].'</option>
			<option value="1">'.$config['RU']['SELECT']['INFO'].'</option>
			<option value="2">'.$config['RU']['SELECT']['WARNING'].'</option>
			<option value="3">'.$config['RU']['SELECT']['ERROR'].'</option>
			<option value="4">'.$config['RU']['SELECT']['DEBUG'].'</option>
		</select><br/>
		<label class="settings_row">Пропускать строки с ошибками</label><input type="checkbox" name="passerrors" id="passerrors" checked value="1"><br/>
		<button type="submit" name="submit" id="submit">Обработать</button>&nbsp;<button type="submit" name="stop" id="stop">Остановить</button>
	</form>
	<div id="progress"></div>
	<div id="messages"></div>
';


//FILES LIST
//Header
$config['html']['files']['header'] = '
<form method="post" action="/mp-spasibo/mp/files/" name="form" enctype="multipart/form-data">
<input type="hidden" name="type" value="files">
<input type="hidden" name="action" value="search">
<label>'.$config['RU']['SEARCH']['LABEL'].'</label><input type="text" name="search" value="%%search%%" /><button type="submit">'.$config['RU']['SEARCH']['BUTTON'].'</button><button type="reset" id="reset">'.$config['RU']['RESET']['BUTTON'].'</button></br></br>
<script>
	$("#reset").click(function(){
			$("[name=search]").val("");
			$("[name=form]").submit();
			return false;
		}
	);
</script>
</form>
<table width="80%" border="1px solid #CEE" cellspacing="0" cellpadding="0">
<tbody>
%%header%%
</tbody>
';
//Each Row
$config['html']['files']['row'] = '
%%row%%
';
//Footer
$config['html']['files']['footer'] = '
</table>
%%footer%%
';

//Custom fields values
$config['html']['partner']['brand_type'] = array(1=>"Онлайн магазин", 2=>"Ретейл", 3=>"Онлайн и ретейл");
$config['html']['partner']['partner_type'] = array(1=>"Начисляющий", 2=>"Списывающий", 3=>"Начисляющий и списывающий");
$config['html']['partner']['category'] = array(1 => "Красота и здоровье",
											2 => "Электроника и бытовая техника",
											3 => "Спортивные товары",
											4 => "Образование",
											5 => "Без категории",
											6 => "Страхование",
											7 => "Товары для животных",
											8 => "Часы и аксессуары",
											9 => "Спорт",
											10 => "Ювелирные украшения",
											11 => "Цветы",
											12 => "Туризм",
											13 => "Товары для дома",
											14 => "Такси",
											15 => "Строительство и ремонт",
											16 => "Рестораны",
											17 => "Развлечения",
											18 => "Продукты",
											19 => "Платежные системы",
											20 => "Оптика",
											21 => "Одежда и обувь",
											22 => "Недвижимость",
											23 => "Сотовая связь",
											24 => "Мебель",
											25 => "Книги",
											26 => "Заправочные станции",
											27 => "Доставка товаров на дом",
											28 => "Детские товары",
											29 => "Все товары в одном магазине",
											30 => "Билеты",
											31 => "Банки",
											32 => "Автомобили",
											33 => "Образовательные центры",
											34 => "Туризм и отдых",
											35 => "Мобильные телефоны и связь",
											36 => "Строительные материалы"
											);



//File settings
$config['files']['mp']['upload_dir'] = "./upload/mp/";
$config['files']['terminals']['upload_dir'] = "./upload/terminals/";
$config['files']['update_file_extension'] = '.updated.csv';
$config['files']['log_file_extension'] = '.log.csv';

//MYSQL SETTINGS
$config['mysql_host'] = "127.0.0.1";
$config['mysql_db'] = "mp-spasibo";
$config['mysql_user'] = "root";
$config['mysql_password'] = "";

//Items in table per page
$config['items_per_page'] = 10;

//Partners Fields list View
//All field names can be found in table 'partners'
$config['partners']['view'] = array('id', 'tsp_name', 'brand_name', 'category', 'brand_type', 'partner_type', 'update_time', "tos_count");
//Partner Fields Required 
//All field names can be found in table 'partners'
$config['partner']['required'] = array('tsp_name', 'brand_name', 'category', 'brand_type', 'partner_type');

//TO Fields list View
//All field names can be found in table 'tos'
$config['tos']['view'] = array('id', 'id_tsp', 'country', 'locality', 'street', 'house', 'lon', 'lat', 'update_time');
//TO Fields Required 
//All field names can be found in table 'tos'
$config['to']['required'] = array('id_tsp', 'country', 'locality', 'street', 'house', 'lon', 'lat');
//TO Rules for fields update in DB
$config['to']['update_rules']['summary_address'] = array("country", "admin_area", "subadmn_area", "locality", "street", "house", "building", "house_add");
//Partner & TO fields to update for each line in csv-file
$config['csv']['update_rules']['line'] = array(
		"id" => "id",
		"country" => "country",
		"admin_area" => "admin_area",
		"subadmin_area" => "subadmin_area",
		"locality" => "locality",
		"street" => "street",
		"house" => "house",
		"building" => "building",
		"house_add" => "house_add",
		"tsp_name" => "tsp_name",
		"category" => "category",
		"brand_name" => "brand_name",
		"brand_description" => "brand_description",
		"contact_phone" => "contact_phone",
		"work_time" => "work_time",
		"action_start" => "action_start",
		"action_stop" => "action_stop",
		"work_flag" => "work_flag",
		"lon" => "lon",
		"lat" => "lat",
		"action_description" => "action_description",
		"brand_url" => "brand_url",
		"brand_facebook" => "brand_facebook",
		"brand_type" => "brand_type",
		"partner_type" => "partner_type",
		"logo" => "logo",
	);
$config['csv']['update_rules']['create_partner'] = true;
$config['csv']['update_rules']['delete_error_line_from_result'] = true;


//CACHE update timeout
$config['cache']['timeout'] = 60*60*24*365; //1 year

//CSV IMPORT SETTINGS
$config['csv']['import']['header_numlines'] = 2;
$config['csv']['import']['fields'] = array(
		0 => "index",
		1 => "id_tsp",
		2 => "country",
		3 => "admin_area",
		4 => "subadmin_area",
		5 => "locality",
		6 => "street",
		7 => "house",
		8 => "building",
		9 => "house_add",
		10 => "tsp_name",
		11 => "category",
		12 => "brand_name",
		13 => "brand_description",
		14 => "contact_phone",
		15 => "work_time",
		16 => "action_start",
		17 => "action_stop",
		18 => "work_flag",
		19 => "lon",
		20 => "lat",
		21 => "action_description",
		22 => "brand_url",
		23 => "brand_facebook",
		24 => "update_time",
		25 => "brand_type",
		26 => "partner_type",
		27 => "logo"
	);

?>