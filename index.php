<?php
require 'config.php';
include 'src/autoload.php';

use MPSpasibo\Partner;
use MPSpasibo\PartnerList;
use MPSpasibo\TO;
use MPSpasibo\TOList;
use MPSpasibo\PDOQuery;
use ForceUTF8\Encoding;
use Goodby\CSV\Import\Standard\Lexer;
use Goodby\CSV\Import\Standard\Interpreter;
use Goodby\CSV\Import\Standard\LexerConfig;


$app = new \Slim\Slim(array(
    'debug' => true,
    'view' => new \MPSpasibo\CustomView()
));

$app->view->setTemplatesDirectory('templates');

$app->log->setWriter(new \MPSpasibo\LogWriter($app->request->isAjax()));
$app->getLog()->getWriter()->setAppLog('./logs/'.'log-'.date('Ymd').'.csv');

$app->group("/", function () use ($app, $config) {
	$app->view->set('%%title%%', 'Главная страница');
	$app->get("", function () use ($app) {
		$app->log->notice("\\root");
	    showPage();
	});
	$app->group("mp/", function () use ($app, $config) {
		$app->view->set('%%title%%', 'Обработка файла для МП');

		$app->get("", function () use ($app, $config) {
			$app->getLog()->notice("\mp");

		    $body = mpShow();
		    $app->view->appendData(array("%%body%%" => $body));
		    showPage();
		});

		$app->map("upload/", function () use ($app, $config) {
		    // echo "\mp\uploading file";
			$app->getLog()->notice("\mp\uploading file");
			uploadFile("mp");
			exit;
		})
			->via('POST')
			;
		$app->group("update/", function () use ($app) {
		    // echo "\update";
			$app->get("", function () use ($app) {
				$app->getLog()->notice("\mp\update");
			    echo "\mp\update";
			});
			$app->get(":skip/:csv_file", function ($skip, $csv_file) use ($app) {
				$app->getLog()->notice("\mp\update");
			    mpParseCSV($skip, $csv_file, $app->request->params());
			});
		});
		$app->get("download/", function () use ($app) {
			$app->getLog()->notice("\mp\download file");
		    echo "\mp\download file";
		});
		$app->group("files/", function () use ($app, $config) {
			$app->map("", function () use ($app, $config) {
				$app->getLog()->notice("\mp\\files list");
			    $body = mpFileShow("mp", $config['files']['mp']['upload_dir'], $app->request->params());
				$app->view->set('%%title%%', 'Список файлов для обработки');
				$app->view->set('%%body%%', $body);
			    showPage();
			})
			->via('POST')
			->via('GET')
			;
			$app->get("delete/:name", function ($name) use ($app, $config) {
				$app->getLog()->notice("\mp\\delete file ".$name);
			    // echo "\mp\delete file ".$name;
			    mpFileDelete($config['files']['mp']['upload_dir'], $name);
			    $body = mpFileShow("mp", $config['files']['mp']['upload_dir']);
				$app->view->set('%%title%%', 'Список файлов для обработки');
				$app->view->set('%%body%%', $body);
			    showPage();
			});
		});
	});

	$app->group("partners/", function () use ($app, $config) {
		$app->map("", function () use ($app, $config) {
			$app->log->notice("\mp\\partners list");
		    $body = partnersShow($app->request->params());
			$app->view->set('%%title%%', 'Управление партнёрами: Список');
			$app->view->set('%%body%%', $body);
		    showPage();
		})
		->via('GET')
		->via('POST')
		;
	});

	$app->group("partner/", function () use ($app, $config) {
		$app->get("add/", function () use ($app, $config) {
			$app->log->notice("\mp\\partner add");
		    $body = partnerNew($app->request->params());
			$app->view->set('%%title%%', 'Управление партнёрами: Добавить нового');
			$app->view->set('%%body%%', $body);
		    showPage();
		});

		$app->post("save/", function () use ($app, $config) {
			$app->log->notice("\mp\\partner save");
		    $body = partnerSave($app->request->params());
			$app->view->set('%%title%%', 'Управление партнёрами: Сохранение');
			$app->view->set('%%body%%', $body);
		    showPage();
		});
		$app->get("edit/:id/", function ($id) use ($app, $config) {
			$app->log->notice("\mp\\partner edit");
		    $body = partnerEdit($id);
			$app->view->set('%%title%%', 'Управление партнёрами: Редактирование');
			$app->view->set('%%body%%', $body);
		    showPage();
		});
		$app->get("delete/:id/", function ($id) use ($app, $config) {
			$app->log->notice("\mp\\partner delete");
		    partnerDelete($id);
		    $body = partnersShow();
			$app->view->set('%%title%%', 'Управление партнёрами: Удаление');
			$app->view->set('%%body%%', $body);
		    showPage();
		});
	});

	$app->group("tos/", function () use ($app, $config) {
		$app->map("", function () use ($app, $config) {
			$app->log->notice("\mp\\tos list");
		    $body = tosShow($app->request->params());
			$app->view->set('%%title%%', 'Управление ТО: Список');
			$app->view->set('%%body%%', $body);
		    showPage();
		})
		->via('GET')
		->via('POST')
		;
	});

	$app->group("to/", function () use ($app, $config) {
		$app->get("add/", function () use ($app, $config) {
			$app->log->notice("\mp\\to add");
		    $body = toNew($app->request->params());
			$app->view->set('%%title%%', 'Управление ТО: Добавить новое');
			$app->view->set('%%body%%', $body);
		    showPage();
		});

		$app->post("save/", function () use ($app, $config) {
			$app->log->notice("\mp\\to save");
		    $body = toSave($app->request->params());
			$app->view->set('%%title%%', 'Управление ТО: Сохранение');
			$app->view->set('%%body%%', $body);
		    showPage();
		});
		$app->get("edit/:id/", function ($id) use ($app, $config) {
			$app->log->notice("\mp\\to edit");
		    $body = toEdit($id);
			$app->view->set('%%title%%', 'Управление ТО: Редактирование');
			$app->view->set('%%body%%', $body);
		    showPage();
		});
		$app->get("delete/:id/", function ($id) use ($app, $config) {
			$app->log->notice("\mp\\to delete");
		    toDelete($id);
		    $body = tosShow();
			$app->view->set('%%title%%', 'Управление ТО: Удаление');
			$app->view->set('%%body%%', $body);
		    showPage();
		});
	});
});

$app->run();

exit;


/*
	    ________  ___   ______________________  _   _______
	   / ____/ / / / | / / ____/_  __/  _/ __ \/ | / / ___/
	  / /_  / / / /  |/ / /     / /  / // / / /  |/ /\__ \ 
	 / __/ / /_/ / /|  / /___  / / _/ // /_/ / /|  /___/ / 
	/_/    \____/_/ |_/\____/ /_/ /___/\____/_/ |_//____/  
	                                                       
*/

function uploadFile($type = "mp"){
	global $config, $_FILES, $app;

	if ($_FILES['csv_file']){

/*
	   __  __      __                __   _____ __   
	  / / / /___  / /___  ____ _____/ /  / __(_) /__ 
	 / / / / __ \/ / __ \/ __ `/ __  /  / /_/ / / _ \
	/ /_/ / /_/ / / /_/ / /_/ / /_/ /  / __/ / /  __/
	\____/ .___/_/\____/\__,_/\__,_/  /_/ /_/_/\___/ 
	    /_/                                          
*/


		$csv_file = $_FILES['csv_file']['tmp_name'];
		$name = $_FILES['csv_file']['name'];
		$name = iconv('UTF-8', 'CP1251', $_FILES['csv_file']['name']);

		if (move_uploaded_file($csv_file, $config['files'][$type]['upload_dir'].$name)){
			$csv_file = $config['files'][$type]['upload_dir'].$name;
			$app->getLog()->info(array('line'=>"", 'message'=>$config['RU']['INFO']['FILE_UPLOADED']));
			$error = false;
		}else{
	        switch ($_FILES['csv_file']['error']) { 
	            case UPLOAD_ERR_INI_SIZE: 
	                $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini"; 
	                break; 
	            case UPLOAD_ERR_FORM_SIZE: 
	                $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form"; 
	                break; 
	            case UPLOAD_ERR_PARTIAL: 
	                $message = "The uploaded file was only partially uploaded"; 
	                break; 
	            case UPLOAD_ERR_NO_FILE: 
	                $message = "No file was uploaded"; 
	                break; 
	            case UPLOAD_ERR_NO_TMP_DIR: 
	                $message = "Missing a temporary folder"; 
	                break; 
	            case UPLOAD_ERR_CANT_WRITE: 
	                $message = "Failed to write file to disk"; 
	                break; 
	            case UPLOAD_ERR_EXTENSION: 
	                $message = "File upload stopped by extension"; 
	                break; 

	            default: 
	                $message = "Unknown upload error"; 
	                break; 
	        } 
	        $csv_file = $message; 
			$app->getLog()->critical(array('line'=>"", 'message'=>$config['RU']['ERRORS']['FILE_UPLOAD_FAILED'].$message));
			$error = true;
		}
		$json = json_encode(array(
		  'name' 	=> $_FILES['csv_file']['name'],
		  'type' 	=> $_FILES['csv_file']['type'],
		  'dataUrl'	=> $csv_file,
		  'error' 	=> $error,
		));
		echo $json;
		$app->getLog()->debug(array('line'=>"", 'message'=>$json));
		flush();
	}
}

function mpParseCSV($skip, $csv_file, $params = array()){
	global $config, $app;

	define('START_TIME', time());
	define('TIMEOUT', 20);
/*
	    ____                          ____________    __   ____              __  _______ 
	   / __ \____ ______________     / ____/ ___/ |  / /  / __/___  _____   /  |/  / __ \
	  / /_/ / __ `/ ___/ ___/ _ \   / /    \__ \| | / /  / /_/ __ \/ ___/  / /|_/ / /_/ /
	 / ____/ /_/ / /  (__  )  __/  / /___ ___/ /| |/ /  / __/ /_/ / /     / /  / / ____/ 
	/_/    \__,_/_/  /____/\___/   \____//____/ |___/  /_/  \____/_/     /_/  /_/_/      
	                                                                                     
*/

		$skip = (int) $skip;
		$csv_file = $config['files']['mp']['upload_dir'].$csv_file;
		$inputEncoding = (string)$params['ie'];
		$exportEncoding = (string)$params['ee'];
		$testmode = (strtolower((string)$params['testmode']) == 'y' ? true : false);
		
		
		if (strpos($csv_file, '.csv') === false) {
			$app->getLog()->critical(array('line'=>"", 'message'=>$config['RU']['ERRORS']['NOT_CSV_FILE']));
			echo 'self.stopConnection();';
			flush();
			exit;
		}

        $fileLog = str_replace('.csv', $config['files']['log_file_extension'], $csv_file);
		$app->getLog()->getWriter()->setFileLog($fileLog);
		$app->getLog()->info($app->getLog()->getWriter()->getFileLog());

		try{
			$lex_config = new LexerConfig();
			$lex_config
			    ->setDelimiter(";") // Customize delimiter. Default value is comma(,)
			    ->setEnclosure("\"")  // Customize enclosure. Default value is double quotation(")
			    //->setIgnoreHeaderLine(true) 
			    // ->setEscape("\\")    // Customize escape character. Default value is backslash(\)
			    // ->setToCharset('cp1251') // Customize target encoding. Default value is null, no converting.
			    //->setFromCharset('cp1251') // Customize CSV file encoding. Default value is null.
			;
			$lexer = new Lexer($lex_config);

			$interpreter = new Interpreter();
			$i = $gLines = 0; $start_time = time();
			$msg = $filename = "";
			if ($skip == 0){
				$app->getLog()->info(array('line'=>"", 'message'=>$config['RU']['INFO']['START']));
			}

			$interpreter->addObserver(function(array $row, $lineNumber, $lines) use (&$i, &$skip, $start_time, $config, &$msg, $csv_file, &$gLines, $inputEncoding, $exportEncoding, $testmode, &$app, &$filename) {
				
				$filename = str_replace(array(".csv", ".xls", ".xslx"), $config['files']['update_file_extension'], $csv_file);
				$gLines = $lines;
				$oldId = 0; // Var contains Id if address was found at cache and it need to check at Yandex 

				// Skip header
				if (!empty($config['csv']['import']['header_numlines']) && $skip == 0 && $i < $config['csv']['import']['header_numlines']){
					foreach ($row as $key => $value) {
						$partner[$config['csv']['import']['fields'][$key]] = $value;
					}

					if ($i == 0){
						$partner['::LOG'] = 'log';
						$partner['::LOG_DESC'] = 'log_desc';
					}else{
						$partner['::LOG'] = '';
						$partner['::LOG_DESC'] = '';
					}

					$encodingString = implode(" ", $partner);

					if (empty($inputEncoding)){
						$inputEncoding = Encoding::detectEncoding($encodingString, strlen($encodingString)); // Encoding of input file
					}

					if (empty($exportEncoding)){
						$exportEncoding = $inputEncoding;
					}

					$lex_config = new Goodby\CSV\Export\Standard\ExporterConfig();
					$lex_config
						->setFileMode(($skip == 0 ? "w+" : "a+")) //Open for writing only; place the file pointer at the end of the file. If the file does not exist, attempt to create it.
						->setFromCharset($inputEncoding)
						->setToCharset($exportEncoding)
						->setDelimiter(";")
					; 

					$exporter = new Goodby\CSV\Export\Standard\Exporter($lex_config);

					//Clean output
					unset($partner['search_address'], $partner['summary_address']);
					$exporter->export($filename, array(
					    $partner,
					));

					$app->getLog()->info(array('line'=>$lineNumber, 'message'=>$config['RU']['INFO']['HEADER_PASSED']));
					$exporter = null;

				}else{
					if ($skip <= $i){
						foreach ($row as $key => $value) {
							$partner[$config['csv']['import']['fields'][$key]] = $value;
						}
						$partner['::LOG'] = '';
						$partner['::LOG_DESC'] = '';
						

						$_str = "";
						if (!empty($config['to']['update_rules']['summary_address'])){
							foreach ($config['to']['update_rules']['summary_address'] as $r_key => $r_value) {
								$_str .= $partner[$r_value]." ";
							}
							$partner['search_address'] = trim($_str);
							$_str = str_replace(array(".",",","/","-","_","(",")","*","%","!","?","+","=",":","`","'","\"","<",">","]","[","#","{","}"),
							 					"", 
							 					$_str);
							$_str = preg_replace('/\p{Zs}/', '', $_str);
							$partner['summary_address'] = trim($_str);
						}else{
							$app->getLog()->critical(array('line'=>$lineNumber, 'message'=>$config['RU']['ERRORS']['CONFIG_ADDRESS_RULES']));
							echo 'updateProgress("'.$config['RU']['CSV']['PROGRESS'].'", '.$i.', '.($gLines+1).');';
							echo 'self.stopConnection();';
							flush();
							exit;
						}

						$encodingString = implode(" ", $partner);

						if (empty($inputEncoding)){
							$inputEncoding = Encoding::detectEncoding($encodingString, strlen($encodingString)); // Encoding of input file
						}

						if (strtolower($inputEncoding) != "utf-8"){
							foreach ($partner as $key => $value) {
								$partner[$key] = Encoding::toUTF8($value, $inputEncoding);
							}
							$partner['summary_address'] = preg_replace('/\p{Zs}/u', '', $partner['summary_address']);
						}

						$line_str = "[".$lineNumber.'] "'.$partner['search_address'].'" ';
						$app->getLog()->debug(array('line'=>$lineNumber, 'message'=>$partner['search_address']." inputEncoding: ".$inputEncoding));

						//Empty Partner information
						if (empty($partner['tsp_name']) && empty($partner['brand_name'])){
							$app->getLog()->error(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".$config['RU']['ERRORS']['NULL_PARTNER']));
							echo 'if (!$("#passerrors").prop("checked")) self.stopConnection();';
							echo 'updateProgress("'.$config['RU']['CSV']['PROGRESS'].'", '.$i.', '.($lines+1).');';
							echo 'self.skip = '.($i+1).';';
							flush();
							exit;
						}

						// Empty address
						if (empty($partner['summary_address'])){
							$app->getLog()->error(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".$config['RU']['ERRORS']['NULL_ADDRESS']));
							echo 'self.skip = '.($i+1).';';
							echo 'updateProgress("'.$config['RU']['CSV']['PROGRESS'].'", '.$i.', '.($gLines+1).');';
							echo 'if (!$("#passerrors").prop("checked")){ self.stopConnection();}';
							flush();
							exit;
						}

						// Search results from DB
						$result = array();

						$to = new TO();
						$cachePartner = $to->searchTO(array("summary_address"=>"=".$partner['summary_address']));

						if ($cachePartner !== false && !empty($cachePartner)){
							if (gettype($cachePartner) == "array"){
								$result = array();
								$result['AdministrativeAreaName'] = $cachePartner[0]["admin_area"];
								$result['Country'] = $cachePartner[0]["country"];
								$result['SubAdministrativeAreaName'] = $cachePartner[0]["subadmin_area"];
								$result['LocalityName'] = $cachePartner[0]["locality"];
								$result['DependentLocalityName'] = "";
								$result['ThoroughfareName'] = $cachePartner[0]['street'];
								$result['PremiseNumber'] = $cachePartner[0]['house'];
								$result['Latitude'] = $cachePartner[0]["lat"]; // широта для исходного запроса
								$result['Longitude'] = $cachePartner[0]["lon"]; // широта для исходного запроса
								$result['house_add'] = $cachePartner[0]["house_add"]; // долгота для исходного запроса
								$result['building'] = $cachePartner[0]["building"]; // долгота для исходного запроса
								$oldId = $cachePartner[0]['id'];
								
								$partner['admin_area'] = $result['AdministrativeAreaName'];
								$partner['country'] = $result['Country'];
								$partner['subadmin_area'] = $result['SubAdministrativeAreaName'];
								$partner['locality'] = $result['LocalityName'];
								$partner['street'] = $result['ThoroughfareName'];
								$partner['house'] = $result['PremiseNumber'];
								$partner['lat'] = $result['Latitude']; // широта для исходного запроса
								$partner['lon'] = $result['Longitude']; // долгота для исходного запроса
								$partner['summary_address'] = $partner['summary_address'];
								$partner['house_add'] = $result['house_add'];
								$partner['building'] = $result['building'];

								$partner['::LOG'] = 'FOUND IN CACHE';
								if (count($cachePartner) > 1){
									$partner['::LOG'] = 'ERROR';
									$partner['::LOG_DESC'] = $config['RU']['ERRORS']['TO_CACHE_FOUND_MORE'];
									$app->getLog()->error(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".$config['RU']['ERRORS']['TO_CACHE_FOUND_TOO_MUCH']));
								}
								if (time() - strtotime($cachePartner[0]['geo_update_time']) > $config['cache']['timeout']){
									// Update geo from Yandex because update time very old
									$result = array();
									$app->getLog()->debug(array('line'=>$lineNumber, 'message'=>$partner['search_address']." "." oldID: ".$oldId));

									$partner['::LOG_DESC'] = $config['RU']['ERRORS']['TO_CACHE_TIMEOUT'];
									$app->getLog()->notice(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".$config['RU']['ERRORS']['TO_CACHE_TIMEOUT']));
								}else{
									$app->getLog()->notice(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".$config['RU']['INFO']['CACHE_FOUND']));
								}
							}
						}else{
							$app->getLog()->error(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".PDOQuery::getInstance()->getError()));
						}

						$insertDataTO = false;

						if (empty($result) || empty($result['Longitude']) || empty($result['Latitude'])){
						// Get from Yandex
							$app->getLog()->notice(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".$config['RU']['INFO']['YANDEX_SEARCH']));

							if (!$testmode){
								$api = new \Yandex\Geo\Api();

								// Настройка фильтров
								$api
									->setQuery($partner['search_address'])
								    ->setLimit(1) // кол-во результатов
								    ->setLang(\Yandex\Geo\Api::LANG_RU) // локаль ответа
								    ->load();

								$response = $api->getResponse();

								// Список найденных точек
								$partner['::LOG'] = 'SEARCHED AT YANDEX';

								if ($response->getFoundCount() > 0){
									$collection = $response->getList();
									$result['AdministrativeAreaName'] = $collection[0]->getAdministrativeAreaName();
									$result['Country'] = $collection[0]->getCountry();
									$result['SubAdministrativeAreaName'] = $collection[0]->getSubAdministrativeAreaName();
									$result['LocalityName'] = $collection[0]->getLocalityName();
									$result['DependentLocalityName'] = $collection[0]->getDependentLocalityName();
									$result['ThoroughfareName'] = $collection[0]->getThoroughfareName();
									$result['PremiseNumber'] = $collection[0]->getPremiseNumber();
									$result['Latitude'] = $collection[0]->getLatitude(); // широта для исходного запроса
									$result['Longitude'] = $collection[0]->getLongitude(); // долгота для исходного запроса

									$insertDataTO = array();
									if ($oldId) $insertDataTO['id'] = (int) $oldId;
									$app->getLog()->debug(array('line'=>$lineNumber, 'message'=>$partner['search_address']." "." insertDataTO[id]: ".$insertDataTO['id']));
									$insertDataTO['admin_area'] = $result['AdministrativeAreaName'];
									$insertDataTO['country'] = $result['Country'];
									$insertDataTO['subadmin_area'] = $result['SubAdministrativeAreaName'];
									$insertDataTO['locality'] = $result['LocalityName'];
									$insertDataTO['street'] = $result['ThoroughfareName'];
									$insertDataTO['house'] = $result['PremiseNumber'];
									$insertDataTO['lat'] = $result['Latitude']; // широта для исходного запроса
									$insertDataTO['lon'] = $result['Longitude']; // долгота для исходного запроса
									$insertDataTO['geo_update_time'] = "LOCALTIMESTAMP()"; 
									$insertDataTO['house_add'] = $partner['house_add'];
									$insertDataTO['building'] = $partner['building'];
									$insertDataTO['summary_address'] = $partner['summary_address'];
								}else{
									$partner['::LOG'] = 'FAILED SEARCH AT YANDEX';
									$partner['::LOG_DESC'] = $config['RU']['ERRORS']['YANDEX_NO_MATCH'].": ".$partner['search_address'];

									$app->getLog()->error(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".$config['RU']['INFO']['YANDEX_NO_MATCH'].": ".$partner['search_address']));
								}
							}else{
								$partner['::LOG'] = 'SEARCH AT YANDEX: TESTMODE';
								$partner['::LOG_DESC'] = '';

								$result = array();
								$result['AdministrativeAreaName'] = "TESTMODE: admin_area";
								$result['Country'] = "TESTMODE: country";
								$result['SubAdministrativeAreaName'] = "TESTMODE: subadmin_area";
								$result['LocalityName'] = "TESTMODE: locality";
								$result['DependentLocalityName'] = "TESTMODE: deplocality";
								$result['ThoroughfareName'] = 'TESTMODE: street';
								$result['PremiseNumber'] = 'TESTMODE: house';
								$result['Latitude'] = "TESTMODE: lat"; 
								$result['Longitude'] = "TESTMODE: long"; 
								$result['house_add'] = "TESTMODE: house_add"; 
								$result['building'] = "TESTMODE: building"; 

								$insertDataTO = array();
								$insertDataTO['admin_area'] = $result['AdministrativeAreaName'];
								$insertDataTO['country'] = $result['Country'];
								$insertDataTO['subadmin_area'] = $result['SubAdministrativeAreaName'];
								$insertDataTO['locality'] = $result['LocalityName'];
								$insertDataTO['street'] = $result['ThoroughfareName'];
								$insertDataTO['house'] = $result['PremiseNumber'];
								$insertDataTO['lat'] = $result['Latitude']; // широта для исходного запроса
								$insertDataTO['lon'] = $result['Longitude']; // долгота для исходного запроса
								$insertDataTO['geo_update_time'] = "LOCALTIMESTAMP()"; 
								$insertDataTO['house_add'] = $partner['house_add'];
								$insertDataTO['building'] = $partner['building'];
								$insertDataTO['summary_address'] = $partner['summary_address'];

								$app->getLog()->warning(array('line'=>$lineNumber, 'message'=>$partner['search_address']." "."TESTMODE: Search at Yandex"));
							}
						}

						//Update partner data
						$_partners = new Partner();
						$search = array();
						if (!empty($partner['id'])){
							$_search["id_tsp"] = (int) $partner['id'];
						}
						if(!empty($partner['tsp_name'])){
							$_search["tsp_name"] = "'".htmlentities(trim($partner['tsp_name']), ENT_QUOTES, "UTF-8")."'";
						}
						if(!empty($partner['brand_name'])){
							$_search["brand_name"] = "'".htmlentities(trim($partner['brand_name']), ENT_QUOTES, "UTF-8")."'";
						}

						$partnerInfo = $_partners->searchPartner($_search, array(), "OR", true);
						if (!empty($partnerInfo)){
							$pId = $pTSP = $pBrand = false;
							foreach ($partnerInfo as $key => $_partner) {
								//Check by ID_TSP
								if ($_partner['id_tsp'] == $partner['id']){
									$pId = $_partner;
								}
								//Check by TSP_NAME
								if ($_partner['tsp_name'] == $partner['tsp_name']){
									$pTSP = $_partner;
								}
								//Check by BRAND_NAME
								if ($_partner['brand_name'] == $partner['brand_name']){
									$pBrand = $_partner;
								}

								unset($pId['id_tsp'], $pTSP['id_tsp'], $pBrand['id_tsp']);
								if ($pBrand) $partner = array_merge($partner, $pBrand);
								elseif ($pId) $partner = array_merge($partner, $pId);
								elseif ($pTSP) $partner = array_merge($partner, $pTSP);
							}
						}else{
							$app->getLog()->warning(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".$config['RU']['WARNING']['NO_PARTNER']));
							if ($config['csv']['update_rules']['create_partner']){
								$insertDataPartner = $partner;
								// Clean partner data
								if (empty($config['__cache']['partner_columns'])){
									$cPartner = new Partner();
									$config['__cache']['partner_columns'] = $cPartner->getColumnsName();
									$cPartner = null;
								}
								unset($insertDataPartner['::LOG'], $insertDataPartner['::LOG_DESC'], $insertDataPartner['id']);
								$unsetKeys = array_diff_key($insertDataPartner, $config['__cache']['partner_columns']);
								foreach ($unsetKeys as $key => $value) {
									unset($insertDataPartner[$key]);
								}

								//Normalize data
								$_insertDataPartner = $insertDataPartner;
								foreach ($_insertDataPartner as $key => $value) {
									if ($key == "category" && !empty($value)){
										$_ct = explode(",", $value);
										$_category = array();
										foreach ($_ct as $_cat) {
											if ($_key = array_search($_cat, $config['html']['partner']['category'])){
												$_category[] = $_key;
											}else{
												$app->getLog()->warning(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".$config['RU']['WARNING']['PARTNER_CREATE_CATEGORY_NULL'].$_cat));
											}
										}
										$value = implode(";", $_category);
									}else{
										$value = str_replace("\"\"", "\"", $value);
									}
									$insertDataPartner[$key] = $value;
								}

								if (!$testmode){
									$cPartner = new Partner($insertDataPartner);
									if ($cPartner->savePartner(true)){
										$app->getLog()->notice(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".$config['RU']['INFO']['PARTNER_CREATED']));
										$_ = $cPartner->values;
										unset($_['id_tsp']);
										$partner = array_merge($partner, $_);
									}else{
										$app->getLog()->debug(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".$cPartner->errors[0]));
										$app->getLog()->error(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".$config['RU']['ERRORS']['FAILED_SAVE']));
									}
								}else{
									$partner = array_merge($partner, $insertDataPartner);
									$app->getLog()->warning(array('line'=>$lineNumber, 'message'=>$partner['search_address']." "."TESTMODE: " .$config['RU']['INFO']['PARTNER_CREATED']));
								}
							}
						}
						if ($insertDataTO){

							if (!$testmode){
								if ($partner['id']) $insertDataTO['id_tsp'] = (int) $partner['id'];
								$to = new TO($insertDataTO);
								$saved = $to->saveTO(true);
								if (!$saved){
									$partner['::LOG'] = 'FAILED SAVE TO DB';
									$app->getLog()->debug(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".$to->errors[0]));
									$app->getLog()->error(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".$config['RU']['ERRORS']['FAILED_SAVE']));
									echo 'updateProgress("'.$config['RU']['CSV']['PROGRESS'].'", '.$i.', '.($lines+1).');';
									echo 'self.skip = '.($i+1).';';
									echo 'if (!$("#passerrors").prop("checked")) self.stopConnection();';
									flush();
									exit;
								}else{
									$app->getLog()->debug(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".$to->values['id']));
									unset($insertDataTO['id_tsp']);
									$partner = array_merge($partner, $insertDataTO);
									unset($partner['id']);
								}

								$app->getLog()->notice(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ".$config['RU']['INFO']['YANDEX_FOUND']));
							}else{
								unset($insertDataTO['id_tsp']);
								$partner = array_merge($partner, $insertDataTO);
								unset($partner['id']);

								$app->getLog()->warning(array('line'=>$lineNumber, 'message'=>$partner['search_address']." "."TESTMODE: ". $config['RU']['INFO']['YANDEX_FOUND']));
							}
						}

						if (empty($result) && $config['csv']['update_rules']['delete_error_line_from_result']){
							$app->getLog()->error(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ". $config['RU']['ERRORS']['FAILED_FIND']));
							echo 'updateProgress("'.$config['RU']['CSV']['PROGRESS'].'", '.$i.', '.($lines+1).');';
							echo 'if (!$("#passerrors").prop("checked")) self.stopConnection();';
							$app->getLog()->notice(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ". $config['RU']['INFO']['LINE_PASSED']));
							echo 'self.skip = '.($i+1).';';
							flush();
							exit;
						}else{
							if (empty($result)) $partner['::LOG'] = 'FAILED FIND DATA FROM CACHE & YANDEX';						
						}

						if (empty($exportEncoding)){
							$exportEncoding = $inputEncoding;
						}
						$lex_config = new Goodby\CSV\Export\Standard\ExporterConfig();
						$lex_config
							->setFileMode("a") //Open for writing only; place the file pointer at the end of the file. If the file does not exist, attempt to create it.
							->setToCharset($exportEncoding)
							->setDelimiter(";")
							; 
						if ($i == 0) $lex_config->setFileMode("w"); //Open for writing only; place the file pointer at the beginning of the file and truncate the file to zero length. If the file does not exist, attempt to create it.
						$exporter = new Goodby\CSV\Export\Standard\Exporter($lex_config);

						//Clean output
						unset($partner['search_address'], $partner['summary_address']);

						$partnerExport = array();
						//Normalize export string
						foreach ($config['csv']['import']['fields'] as $key => $name) {
							if (in_array($name, array("tsp_name", "brand_name", "brand_description", "contact_phone", "work_time", "action_description"))){
								$value = html_entity_decode($partner[$name], ENT_QUOTES, "UTF-8");
							}elseif($name == "update_time"){
								$value = date("d.m.Y");
							}elseif($name == "category" && !empty($partner[$name])){
								$_cat = explode(";", $partner[$name]);
								$value = array();
								foreach ($_cat as $ind) {
									$value[] = html_entity_decode($config['html']['partner']['category'][$ind], ENT_QUOTES, "UTF-8");
								}
								$value = implode(", ", $value);
							}else{
								$value = $partner[$name];
							}
							$partnerExport[$name] = $value;
						}
						$partnerExport['::LOG'] = $partner['::LOG'];
						$partnerExport['::LOG_DESC'] = $partner['::LOG_DESC'];

						$exporter->export($filename, array(
						    $partnerExport,
						));
						$exporter = $lexer = $lex_config = null;
						$app->getLog()->notice(array('line'=>$lineNumber, 'message'=>$partner['search_address']." ". $config['RU']['INFO']['LINE_PASSED']));
					}
				}
				$i++;

				// Check runtime
			    if (time() - $start_time > 10){
					if ($skip < $i){
						echo 'self.skip = '.$i.';';
						echo 'updateProgress("'.$config['RU']['CSV']['PROGRESS'].'", '.$i.', '.($lines+1).');';
					}else{
						$app->getLog()->warning(array('line'=>"", 'message'=>"Не хватило времени на обработку, i=".$i.", skip=".$skip));
					}
					flush();
					exit;
			    }
			});

			$lexer->parse($csv_file, $interpreter);
		}catch(Exception $exp){
			$app->getLog()->critical(array('line'=>$i, 'message'=>$exp->getMessage()));
			echo 'self.skip = '.($i+1).';';
			echo 'updateProgress("'.$config['RU']['CSV']['PROGRESS'].'", '.$i.', '.($gLines+1).');';
			echo 'if (!$("#passerrors").prop("checked")) self.stopConnection();';
			flush();
			exit;
		}

		$filename = str_replace(array(".csv", ".xls", ".xslx"), $config['files']['update_file_extension'], $csv_file);

		echo 'self.stopConnection();';
		$app->getLog()->notice(array('line'=>"", 'message'=>"<a href='".$filename."' target='blank'>".$config['RU']['INFO']['DOWNLOAD']."</a>"));
		$app->getLog()->notice(array('line'=>"", 'message'=>$config['RU']['INFO']['FILE_PASSED']));
		echo 'updateProgress("'.$config['RU']['CSV']['PROGRESS'].'", '.$i.', '.($gLines+1).');';
		echo 'self.skip = '.$i.';';
		echo 'self.downloadFile()';
		flush();
		exit;
}
	// }elseif (!empty($_REQUEST['csv_file']) && $_REQUEST['action'] == 'download'){

function mpDownloadFile(){
	global $config;
/*
	    ____                      __                __   _______ __   
	   / __ \____ _      ______  / /___  ____ _____/ /  / ____(_) /__ 
	  / / / / __ \ | /| / / __ \/ / __ \/ __ `/ __  /  / /_  / / / _ \
	 / /_/ / /_/ / |/ |/ / / / / / /_/ / /_/ / /_/ /  / __/ / / /  __/
	/_____/\____/|__/|__/_/ /_/_/\____/\__,_/\__,_/  /_/   /_/_/\___/ 
	                                                                  
*/

	$filename = str_replace(array(".csv", ".xls", ".xslx"), $config['files']['update_file_extension'], $_REQUEST['csv_file']);
	file_force_download($filename);	
}
	// }else{

function mpShow(){
	global $config;

	// showHead("Формирование файла для МП-Спасибо");
	return $config['html']['mp']['form'];
	// showFooter();
}
	// }

/*
	  ______                    _             __    
	 /_  __/__  _________ ___  (_)___  ____ _/ /____
	  / / / _ \/ ___/ __ `__ \/ / __ \/ __ `/ / ___/
	 / / /  __/ /  / / / / / / / / / / /_/ / (__  ) 
	/_/  \___/_/  /_/ /_/ /_/_/_/ /_/\__,_/_/____/  
	                                                
*/
	
if ( 1 != 0){
}elseif (!empty($_REQUEST['type']) && $_REQUEST['type'] == 'terminals'){
	$array_shops = array('Москва туристкая 19 4', 'новосибирск мусы джалиля 1', 'г. Москва ул. НоваяБасманная д 29.стр.1', ' г. Москва Руновский переулок д 11/13 стр 2');
	$coords_file = array();
	$geo_objects = array();
	define('START_TIME', time());
	define('TIMEOUT', 20);

	if ($_FILES['csv_file']){

/*
	   __  __      __                __   _____ __   
	  / / / /___  / /___  ____ _____/ /  / __(_) /__ 
	 / / / / __ \/ / __ \/ __ `/ __  /  / /_/ / / _ \
	/ /_/ / /_/ / / /_/ / /_/ / /_/ /  / __/ / /  __/
	\____/ .___/_/\____/\__,_/\__,_/  /_/ /_/_/\___/ 
	    /_/                                          
*/


		$csv_file = $_FILES['csv_file']['tmp_name'];
		$name = $_FILES['csv_file']['name'];
		$name = iconv('UTF-8', 'CP1251', $_FILES['csv_file']['name']);
		if (move_uploaded_file($csv_file, $config['files']['terminals']['upload_dir']."$name")){
			$csv_file = $config['files']['terminals']['upload_dir'].$_FILES['csv_file']['name'];
			$error = false;
		}else{
			$csv_file = $_FILES['csv_file']['error'];
	        switch ($_FILES['csv_file']['error']) { 
	            case UPLOAD_ERR_INI_SIZE: 
	                $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini"; 
	                break; 
	            case UPLOAD_ERR_FORM_SIZE: 
	                $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form"; 
	                break; 
	            case UPLOAD_ERR_PARTIAL: 
	                $message = "The uploaded file was only partially uploaded"; 
	                break; 
	            case UPLOAD_ERR_NO_FILE: 
	                $message = "No file was uploaded"; 
	                break; 
	            case UPLOAD_ERR_NO_TMP_DIR: 
	                $message = "Missing a temporary folder"; 
	                break; 
	            case UPLOAD_ERR_CANT_WRITE: 
	                $message = "Failed to write file to disk"; 
	                break; 
	            case UPLOAD_ERR_EXTENSION: 
	                $message = "File upload stopped by extension"; 
	                break; 

	            default: 
	                $message = "Unknown upload error"; 
	                break; 
	        } 
	        $csv_file = $message; 
			$error = true;
		}
		$json = json_encode(array(
		  'name' => $_FILES['csv_file']['name'],
		  'type' => $_FILES['csv_file']['type'],
		  'dataUrl' => $csv_file,
		  'error' => $error,
		));
		echo $json;
		flush();
		exit;

	}elseif (!empty($_REQUEST['csv_file']) && $_REQUEST['action'] == 'update' && empty($_REQUEST['download'])){

/*
	    ____                          ____________    __   ____              ______                    _             __    
	   / __ \____ ______________     / ____/ ___/ |  / /  / __/___  _____   /_  __/__  _________ ___  (_)___  ____ _/ /____
	  / /_/ / __ `/ ___/ ___/ _ \   / /    \__ \| | / /  / /_/ __ \/ ___/    / / / _ \/ ___/ __ `__ \/ / __ \/ __ `/ / ___/
	 / ____/ /_/ / /  (__  )  __/  / /___ ___/ /| |/ /  / __/ /_/ / /       / / /  __/ /  / / / / / / / / / / /_/ / (__  ) 
	/_/    \__,_/_/  /____/\___/   \____//____/ |___/  /_/  \____/_/       /_/  \___/_/  /_/ /_/ /_/_/_/ /_/\__,_/_/____/  
	                                                                                                                       
*/
		$csv_file = (string)$_REQUEST['csv_file'];
		$skip = (int)$_REQUEST['skip'];
		
		try{
			$lex_config = new LexerConfig();
			$lex_config
			    ->setDelimiter(";") // Customize delimiter. Default value is comma(,)
			    ->setEnclosure("\"")  // Customize enclosure. Default value is double quotation(")
			    //->setIgnoreHeaderLine(true) 
			    // ->setEscape("\\")    // Customize escape character. Default value is backslash(\)
			    // ->setToCharset('cp1251') // Customize target encoding. Default value is null, no converting.
			    //->setFromCharset('cp1251') // Customize CSV file encoding. Default value is null.
			;
			$lexer = new Lexer($lex_config);

			$interpreter = new Interpreter();
			$i = $gLines = 0; $start_time = time();

			$msg = "";
			if ($skip == 0)
				echo 'putMessage(0,"'.str_replace('"','\"',$config['RU']['STATUS']['GENERAL']).'","'.str_replace('"','\"', $config['RU']['INFO']['START']).'");';

			$interpreter->addObserver(function(array $row, $lineNumber, $lines) use (&$i, &$skip, $start_time, $config, &$msg, $csv_file, &$gLines) {
				
				$filename = str_replace(array(".csv", ".xls", ".xslx"), $config['files']['update_file_extension'], $csv_file);
				$inputEncoding = "";
				$gLines = $lines;
				$oldId = 0; // Var contains Id if address was found at cache and it need to check at Yandex 

				// Skip header
				if (!empty($config['csv']['import']['header_numlines']) && $skip == 0 && $i < $config['csv']['import']['header_numlines']){
					foreach ($row as $key => $value) {
						$partner[$config['csv']['import']['fields'][$key]] = $value;
					}

					if ($i == 0){
						$partner['::LOG'] = 'log';
						$partner['::LOG_DESC'] = 'log_desc';
					}else{
						$partner['::LOG'] = '';
						$partner['::LOG_DESC'] = '';
					}

					$lex_config = new Goodby\CSV\Export\Standard\ExporterConfig();
					$lex_config
						->setFileMode("a") //Open for writing only; place the file pointer at the end of the file. If the file does not exist, attempt to create it.
						->setDelimiter(";")
					; 
					if ($i == 0) $lex_config->setFileMode("w"); //Open for writing only; place the file pointer at the beginning of the file and truncate the file to zero length. If the file does not exist, attempt to create it.
					$exporter = new Goodby\CSV\Export\Standard\Exporter($lex_config);

					//Clean output
					unset($partner['search_address'], $partner['summary_address']);
					$exporter->export($filename, array(
					    $partner,
					));

					echo 'putMessage(1,"'.str_replace('"','\"',$config['RU']['STATUS']['INFO']).'","'.str_replace('"','\"', "[".$lineNumber."] ".$config['RU']['INFO']['HEADER_PASSED']).'");';
					$exporter = null;

				}else{
					if ($skip <= $i){
						foreach ($row as $key => $value) {
							$partner[$config['csv']['import']['fields'][$key]] = $value;
						}
						$partner['::LOG'] = '';
						$partner['::LOG_DESC'] = '';
					}
				}
			});

			$lexer->parse($csv_file, $interpreter);
		}catch(Exception $exp){
			echo 'putMessage(0,"'.str_replace('"','\"',$config['RU']['STATUS']['ERROR']).'","'.str_replace('"','\"', $line_str . $exp->getMessage()).'");';
			echo 'self.skip = '.($i+1).';';
			echo 'updateProgress("'.$config['RU']['CSV']['PROGRESS'].'", '.$i.', '.($gLines+1).');';
			echo 'if (!$("#passerrors").prop("checked")) self.stopConnection();';
			flush();
			exit;
		}

		$filename = str_replace(array(".csv", ".xls", ".xslx"), $config['files']['update_file_extension'], $csv_file);

		echo 'self.stopConnection();';
		echo 'putMessage(0,"'.str_replace('"','\"',$config['RU']['STATUS']['GENERAL']).'","'.str_replace('"','\"', "<a href='".$filename."' target='blank'>".$config['RU']['INFO']['DOWNLOAD']."</a>").'");';
		echo 'putMessage(0,"'.str_replace('"','\"',$config['RU']['STATUS']['GENERAL']).'","'.str_replace('"','\"', $config['RU']['INFO']['FILE_PASSED']).'");';
		echo 'updateProgress("'.$config['RU']['CSV']['PROGRESS'].'", '.$i.', '.($gLines+1).');';
		echo 'this.skip = '.$i.';';
		echo 'self.downloadFile()';
		flush();
		exit;
	}elseif (!empty($_REQUEST['csv_file']) && $_REQUEST['action'] == 'download'){

/*
	    ____                      __                __   _______ __   
	   / __ \____ _      ______  / /___  ____ _____/ /  / ____(_) /__ 
	  / / / / __ \ | /| / / __ \/ / __ \/ __ `/ __  /  / /_  / / / _ \
	 / /_/ / /_/ / |/ |/ / / / / / /_/ / /_/ / /_/ /  / __/ / / /  __/
	/_____/\____/|__/|__/_/ /_/_/\____/\__,_/\__,_/  /_/   /_/_/\___/ 
	                                                                  
*/

		$filename = str_replace(array(".csv", ".xls", ".xslx"), $config['files']['update_file_extension'], $_REQUEST['csv_file']);
		file_force_download($filename);
		
	}else{
		showHead("Формирование файла по терминалам");
		echo $config['html']['terminals']['form'];
		showFooter();
	}
}

function mpFileDelete($path, $name){
	global $config, $app;
/*
	    _______ __        ____       __     __     
	   / ____(_) /__     / __ \___  / /__  / /____ 
	  / /_  / / / _ \   / / / / _ \/ / _ \/ __/ _ \
	 / __/ / / /  __/  / /_/ /  __/ /  __/ /_/  __/
	/_/   /_/_/\___/  /_____/\___/_/\___/\__/\___/ 
	                                               
*/

	if (unlink($path.$name)){
		$app->getLog()->info($config['RU']['INFO']['FILE_DELETE_SUCCESS'] . ": " . $path.$name);
		$filenameUpdated = str_replace(array(".csv", ".xls", ".xslx"), $config['files']['update_file_extension'], $name);
		if (file_exists($path.$filenameUpdated)){
			unlink($path.$filenameUpdated);
			$app->getLog()->info($config['RU']['INFO']['FILE_DELETE_SUCCESS'] . ": " . $path.$filenameUpdated);
		}else{
			$app->flashNow('error', $config['RU']['INFO']['FILE_DELETE_FAILED'] . ": " . $path.$filenameUpdated);
		}
		$filenameLog = str_replace(array(".csv", ".xls", ".xslx"), $config['files']['log_file_extension'], $name);
		if (file_exists($path.$filenameLog)){
			unlink($path.$filenameLog);
			$app->getLog()->info($config['RU']['INFO']['FILE_DELETE_SUCCESS'] . ": " . $path.$filenameLog);
		}else{
			$app->flashNow('error', $config['RU']['INFO']['FILE_DELETE_FAILED'] . ": " . $path.$filenameLog);
		}
		$app->flashNow('info', $config['RU']['INFO']['FILE_DELETE_SUCCESS']);
	}else{
		$app->flashNow('error', $config['RU']['INFO']['FILE_DELETE_FAILED'] . ": " . $path.$name);
	}
}

function mpFileShow($type, $path, $params = array()){
	global $config, $app;
/*
	    _______ __             __    _      __ 
	   / ____(_) /__  _____   / /   (_)____/ /_
	  / /_  / / / _ \/ ___/  / /   / / ___/ __/
	 / __/ / / /  __(__  )  / /___/ (__  ) /_  
	/_/   /_/_/\___/____/  /_____/_/____/\__/  
	                                           
*/

	$_header = "<th>".$config['RU']['FIELDS']['FILENAME']."</th>";
	$_header .= "<th>".$config['RU']['FIELDS']['FILE_UPDATED']."</th>";
	$_header .= "<th>".$config['RU']['FIELDS']['FILE_LOG']."</th>";
	$_header .= "<th>".$config['RU']['FIELDS']['FILE_CREATED']."</th>";
	$_header .= "<th>".$config['RU']['FIELDS']['FILE_DELETE']."</th>";

	$_search = empty($params['search']) ? "" : $params['search'];

	$_header = str_replace(array("%%header%%", "%%search%%"), array($_header, $_search), $config['html']['files']['header']);
	$_footer = str_replace("%%footer%%", "", $config['html']['files']['footer']);
	$result = $_header;

	foreach (new FilesystemIterator($path) as $fileInfo) {
		$filename = $fileInfo->getFilename();

    	if($fileInfo->isDir() || $filename == '.DS_Store') continue;

    	if (strpos($filename, $config['files']['update_file_extension']) !== false) continue;
    	if (strpos($filename, $config['files']['log_file_extension']) !== false) continue;

    	if (!empty($_search) && strpos($filename, $_search) === false) continue;

    	$_file = "<tr><td><a href='/mp-spasibo/upload/".$type."/".$filename."'>".$filename."</a></td>\n";

		$filenameUpdated = str_replace(array(".csv", ".xls", ".xslx"), $config['files']['update_file_extension'], $filename);
		if (file_exists($path.$filenameUpdated)){
	    	$_file .= "<td><a href='/mp-spasibo/upload/".$type."/".$filenameUpdated."'>".$filenameUpdated."</a></td>\n";
		}else{
	    	$_file .= "<td>&nbsp;</td>\n";
	    }

		$filenameLog = str_replace(array(".csv", ".xls", ".xslx"), $config['files']['log_file_extension'], $filename);
		if (file_exists($path.$filenameLog)){
	    	$_file .= "<td><a href='/mp-spasibo/upload/".$type."/".$filenameLog."'>".$filenameLog."</a></td>\n";
		}else{
	    	$_file .= "<td>&nbsp;</td>\n";
		}
    	$_file .= "<td>".date("d.m.Y H:i:s", $fileInfo->getATime())."</td>\n";

    	$_file .= "<td><a href='/mp-spasibo/".$type."/files/delete/".$filename."' onclick='if (confirm(\"Удалить файл?\")) return true; else return false;'>".$config['RU']['FIELDS']['FILE_DELETE']."</a></td></tr>\n";

		$_row .= str_replace("%%row%%", $_file, $config['html']['files']['row']);
	}
	$result .= $_row;
	$result .= $_footer;
	return $result;
}

function partnerNew($params){
	global $config, $app;	
	/*
	    ____                   __               _   __            
	   / __ \____ __________  / /____  _____   / | / /__ _      __
	  / /_/ / __ `/ ___/ __ \/ __/ _ \/ ___/  /  |/ / _ \ | /| / /
	 / ____/ /_/ / /  / / / / /_/  __/ /     / /|  /  __/ |/ |/ / 
	/_/    \__,_/_/  /_/ /_/\__/\___/_/     /_/ |_/\___/|__/|__/  
	                                                              
*/

	$partner = new Partner();
	$partner->getColumnsName();
	return $partner->showForm();

}

function partnerSave($params){
	global $config, $app;


/*
	    ____             __                     _____                
	   / __ \____ ______/ /_____  ___  _____   / ___/____ __   _____ 
	  / /_/ / __ `/ ___/ __/ __ \/ _ \/ ___/   \__ \/ __ `/ | / / _ \
	 / ____/ /_/ / /  / /_/ / / /  __/ /      ___/ / /_/ /| |/ /  __/
	/_/    \__,_/_/   \__/_/ /_/\___/_/      /____/\__,_/ |___/\___/ 
	                                                                 
*/
			
	unset($params['type'], $params['action']);
	
	$_values = $params;
	$partner = new Partner($_values);
	
	if (empty($_values['tsp_name']) && empty($_values['brand_name'])){
		echo $config['RU']['ERRORS']['TSP_BRAND_NULL'];
	}else{
		if (!$partner->savePartner()){
			$app->flashNow('error', $config['RU']['ERRORS']['PARTNER_SQL_ERROR']);
			$app->getLog()->error($config['RU']['ERRORS']['PARTNER_SQL_ERROR']);
			$app->getLog()->debug($config['RU']['ERRORS']['PARTNER_SQL_ERROR']. ": " . $partner->errors[0] . ", data: " . print_r($params, true));
		}else{
			$app->flashNow('info', $config['RU']['PARTNER']['SAVED']);
			$app->getLog()->info($config['RU']['PARTNER']['SAVED']);
			$app->getLog()->debug($config['RU']['PARTNER']['SAVED'] . ": #" . $params['id']);
		}
	}
	
	return $partner->showForm();
}

function partnerDelete($id){
	global $config, $app;

/*
	    ____             __                     ____       __     __     
	   / __ \____ ______/ /_____  ___  _____   / __ \___  / /__  / /____ 
	  / /_/ / __ `/ ___/ __/ __ \/ _ \/ ___/  / / / / _ \/ / _ \/ __/ _ \
	 / ____/ /_/ / /  / /_/ / / /  __/ /     / /_/ /  __/ /  __/ /_/  __/
	/_/    \__,_/_/   \__/_/ /_/\___/_/     /_____/\___/_/\___/\__/\___/ 
	                                                                     
*/

		// Управление партнёрами: Удаление
	$id = intval($id);
	$partner = new Partner(array('id'=>$id));
	if ($partner->deletePartner()){
		$app->flashNow('info', $config['RU']['PARTNER']['DELETED']);
		$app->getLog()->info($config['RU']['PARTNER']['DELETED'] . ": #" . $id);
	}else{
		$app->flashNow('error', $config['RU']['ERRORS']['PARTNER_SQL_ERROR']);
		$app->getLog()->error($config['RU']['ERRORS']['PARTNER_SQL_ERROR']);
		$app->getLog()->debug($config['RU']['ERRORS']['PARTNER_SQL_ERROR']. ": " . $partner->errors[0] . ", #" . $id);
	}

	return $partner->showForm();
}

function partnerEdit($id){
	global $config, $app;

/*
	    ____             __                     ______    ___ __ 
	   / __ \____ ______/ /_____  ___  _____   / ____/___/ (_) /_
	  / /_/ / __ `/ ___/ __/ __ \/ _ \/ ___/  / __/ / __  / / __/
	 / ____/ /_/ / /  / /_/ / / /  __/ /     / /___/ /_/ / / /_  
	/_/    \__,_/_/   \__/_/ /_/\___/_/     /_____/\__,_/_/\__/  
	                                                             
*/

	$id = intval($id);
	
	$partner = new Partner();
	$partner->loadPartner($id);
	return $partner->showForm();
}

function partnersShow($params = array()){
	global $config, $app;		
			// showHead("Управление партнёрами: Список");

/*
	    ____             __                     _____ __                     __    _      __ 
	   / __ \____ ______/ /_____  ___  _____   / ___// /_  ____ _      __   / /   (_)____/ /_
	  / /_/ / __ `/ ___/ __/ __ \/ _ \/ ___/   \__ \/ __ \/ __ \ | /| / /  / /   / / ___/ __/
	 / ____/ /_/ / /  / /_/ / / /  __/ /      ___/ / / / / /_/ / |/ |/ /  / /___/ (__  ) /_  
	/_/    \__,_/_/   \__/_/ /_/\___/_/      /____/_/ /_/\____/|__/|__/  /_____/_/____/\__/  
	                                                                                         
*/


	$sqlParams = array();
	$sqlExt = $search = "";
	if (!empty($params['action']) && $params['action'] == 'search' && !empty($params['search'])){
		$search = htmlentities($params['search'], ENT_QUOTES, "UTF-8");
		$sqlExt = "WHERE p.tsp_name LIKE '%".$search."%' or p.brand_name LIKE '%".$search."%'";
		// $sqlParams[':search'] = $search;
	}

	$_page = $_pagecount = 0;

	if (!empty($params['page']) && intval($params['page']) > 0){
		$_pagecount = intval($params['page'])-1;
	}
	$_page = $_pagecount * $config['items_per_page'];

	$sql = "SELECT 
		p.*, t.tos_count 
		FROM partners as p
		LEFT JOIN (
			SELECT count(*) AS tos_count, id_tsp FROM tos GROUP BY id_tsp
		) AS t 
		ON t.id_tsp=p.id
		".$sqlExt."
		ORDER BY p.id ASC
		LIMIT :page, :items_per_page
	";

	$sqlParams[':page'] = $_page;
	$sqlParams[':items_per_page'] = $config['items_per_page'];

	PDOQuery::getInstance($config['mysql_user'], $config['mysql_password'], $config['mysql_host'], $config['mysql_db']);
	$rows = PDOQuery::getInstance()->queryPrepared($sql, $sqlParams);
	if ($rows === false){
		$app->flashNow('error', $config['RU']['ERRORS']['PARTNERS_SHOW_ERROR']);
		$app->getLog()->error($config['RU']['ERRORS']['PARTNERS_SHOW_ERROR']);
		$app->getLog()->debug($config['RU']['ERRORS']['PARTNERS_SHOW_ERROR'] . ": " . PDOQuery::getInstance()->getError());
	}else{
		$rows_count = PDOQuery::getInstance()->getRowCount();

		$partners = new PartnerList($rows, $_pagecount, ceil($rows_count/$config['items_per_page']));
 		$partners->search = $search;
		$result = $partners->showPartners();
	}
	return $result;
}

function toNew($params){
	global $config, $app;	
/*
	  __________     _   __            
	 /_  __/ __ \   / | / /__ _      __
	  / / / / / /  /  |/ / _ \ | /| / /
	 / / / /_/ /  / /|  /  __/ |/ |/ / 
	/_/  \____/  /_/ |_/\___/|__/|__/  
	                                   
*/

	$to = new TO();
	$to->getColumnsName();
	return $to->showForm();
}

function toSave($params){
	global $config, $app;			


/*
	  __________     _____                
	 /_  __/ __ \   / ___/____ __   _____ 
	  / / / / / /   \__ \/ __ `/ | / / _ \
	 / / / /_/ /   ___/ / /_/ /| |/ /  __/
	/_/  \____/   /____/\__,_/ |___/\___/ 
	                                      
*/

	unset($params['type'], $params['action']);
	
	$_values = $params;
	$to = new TO($_values);
	
	if (empty($params['id_tsp'])){
		$app->flashNow('error', $config['RU']['ERRORS']['ID_TSP_NULL']);
	}else{
		if (!$to->saveTO()){
			$app->flashNow('error', $config['RU']['ERRORS']['PARTNER_SQL_ERROR']);
			$app->getLog()->error($config['RU']['ERRORS']['PARTNER_SQL_ERROR']);
			$app->getLog()->debug($config['RU']['ERRORS']['PARTNER_SQL_ERROR']. ": " . $to->errors[0] . ", data: " . print_r($params, true));
		}else{
			$app->flashNow('info', $config['RU']['TO']['SAVED']);
			$app->getLog()->info($config['RU']['TO']['SAVED']);
			$app->getLog()->debug($config['RU']['TO']['SAVED'] . ": #" . $params['id']);
		}
	}
	
	return  $to->showForm();
}

function toDelete($id){
	global $config, $app;

/*
	  __________     ____       __     __     
	 /_  __/ __ \   / __ \___  / /__  / /____ 
	  / / / / / /  / / / / _ \/ / _ \/ __/ _ \
	 / / / /_/ /  / /_/ /  __/ /  __/ /_/  __/
	/_/  \____/  /_____/\___/_/\___/\__/\___/ 
	                                          
*/

	$id = intval($id);
	$to = new TO(array('id'=>$id));
	if ($to->deleteTO()){
		$app->flashNow('info', $config['RU']['TO']['DELETED']);
	}else{
		$app->flashNow('error', $config['RU']['TO']['DELETED']);
	}

	return $to->showForm();
}

function toEdit($id){
	global $config, $app;

/*
	  __________     ______    ___ __ 
	 /_  __/ __ \   / ____/___/ (_) /_
	  / / / / / /  / __/ / __  / / __/
	 / / / /_/ /  / /___/ /_/ / / /_  
	/_/  \____/  /_____/\__,_/_/\__/  
	                                  
*/
	$id = intval($id);

	$to = new TO();
	$to->loadTO($id);

	return $to->showForm();
}

function tosShow($params = array()){
	global $config, $app;		

/*
	  __________       _____    __                              __       _            __ 
	 /_  __/ __ \     / ___/   / /_     ____     _      __     / /      (_)  _____   / /_
	  / / / / / /     \__ \   / __ \   / __ \   | | /| / /    / /      / /  / ___/  / __/
	 / / / /_/ /     ___/ /  / / / /  / /_/ /   | |/ |/ /    / /___   / /  (__  )  / /_  
	/_/  \____/     /____/  /_/ /_/   \____/    |__/|__/    /_____/  /_/  /____/   \__/  
	                                                                                     
*/

	$sqlParams = array();
	$sqlExt = $search = $id_tsp = "";
	if (!empty($params['action']) && $params['action'] == 'search' && !empty($params['search'])){
		$search = htmlentities($params['search'], ENT_QUOTES, "UTF-8");
		$sqlExt = "WHERE (t.admin_area LIKE '%".$search."%' or t.street LIKE '%".$search."%' or t.locality LIKE '%".$search."%' or t.subadmin_area LIKE '%".$search."%')";
	}
	if (!empty($params['id_tsp'])){
		$id_tsp = (int)$params['id_tsp'];
		if ($sqlExt){
			$sqlExt .= " and t.id_tsp = '".$id_tsp."'";
		}else{
			$sqlExt = "WHERE t.id_tsp = '".$id_tsp."'";
		}
	}

	$_page = $_pagecount = 0;

	if (!empty($params['page']) && intval($params['page']) > 0){
		$_pagecount = intval($params['page'])-1;
	}
	$_page = $_pagecount * $config['items_per_page'];

	$sql = "
		SELECT 
		t . * , p.tsp_name, p.brand_name
		FROM  `tos` AS t
		LEFT JOIN  `partners` AS p ON t.id_tsp = p.id
		".$sqlExt."
		ORDER BY t.ID ASC
		LIMIT :page, :items_per_page
	";
	$sqlParams[':page'] = $_page;
	$sqlParams[':items_per_page'] = $config['items_per_page'];

	PDOQuery::getInstance($config['mysql_user'], $config['mysql_password'], $config['mysql_host'], $config['mysql_db']);
	$rows = PDOQuery::getInstance()->queryPrepared($sql, $sqlParams);
	if ($rows === false){
		$app->flashNow('error', PDOQuery::getInstance()->getError());
	}else{
		$rows_count = PDOQuery::getInstance()->getRowCount();

		$tos = new TOList($rows, $_pagecount, ceil($rows_count/$config['items_per_page']));
 		$tos->search = $search;
 		$tos->id_tsp = $id_tsp;

		return $tos->showTOs();
	}

	return;
}

//DEPRICATED
function allShowMainPage(){
	global $config;

	showHead("Обработка файла для МП");
	showFooter();
}

function file_force_download($file) {
	if (file_exists($file)) {
		// сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
		// если этого не сделать файл будет читаться в память полностью!
		if (ob_get_level()) {
		  ob_end_clean();
		}
		// заставляем браузер показать окно сохранения файла
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename=' . basename($file));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		// читаем файл и отправляем его пользователю
		if ($fd = fopen($file, 'rb')) {
		  while (!feof($fd)) {
			print(fread($fd, 1024));
		  }
		  fclose($fd);
		}
		exit;
	}
}

function getName($ar){
	global $_result;
	
	foreach($ar as $key => $value){
		if (strpos($key, 'Name') !== false || strpos($key, 'PremiseNumber') !== false){
			$_result[$key] = $value;
		}
		if (is_array($value)){
			getName($value);
		}
	}
}

function showHead($name = ""){
	global $config, $app;

	echo $app->render('header.html');
}

function showBody(){
	global $config, $app;

	if (empty($app->view->get('%%body%%'))){
		$app->view->set('%%body%%', '');
	}
	echo $app->render('body.html');
}

function showFooter(){
	global $config, $app;

	echo $app->render('footer.html');	
}

function showPage(){
	showHead();
	showBody();
	showFooter();
}
?>